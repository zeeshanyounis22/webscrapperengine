﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebScrapper.App_Start;
using WebScrapper.Attributes;
using WebScrapper.Jobs;

namespace WebScrapper
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Scheduler scheduler = new Scheduler();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            HangfireBootstrapper.Instance.Start();
            GlobalJobFilters.Filters.Add(new LogEverythingAttribute());
            scheduler.Start(180);
            JobScheduler.Start();

        }

        protected void Application_End(object sender, EventArgs e)
        {
            HangfireBootstrapper.Instance.Stop();
        }
    }
}
