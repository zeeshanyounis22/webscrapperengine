﻿function datetimeConvertion(input) {
    var output = "";
    // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';
    // At least Safari 3+: "[object HTMLElementConstructor]"
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;
    // Chrome 1+
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    // Blink engine detection
    var isBlink = (isChrome || isOpera) && !!window.CSS;
    var formattedDate;
    var tempdatetime = input;
    if (input != null) {
        input = new Date(input);
        formattedDate = input;
    }

    if (input != null && input.toString() === "Invalid Date") { // checking if input is not null but it contains only time.
        formattedDate = null;
        var val = tempdatetime.toString().split(":");
        input = new Date();
        input.setHours(val[0]);
        input.setMinutes(val[1]);
        input.setSeconds(val[2]);
        formattedDate = moment(input).format('YYYY-MM-DDTHH:mm:ss');
    }

    if ((isChrome || isOpera || isSafari) && formattedDate != null) {
        input = new Date(formattedDate);
    }
    else if ((isEdge || isIE || isFirefox) && formattedDate != null) {

        input = new Date(formattedDate);
        input.setMinutes(input.getMinutes() - (input.getTimezoneOffset()));
    }
    else if (formattedDate != null) {
        input = new Date(formattedDate);
        input.setMinutes(input.getMinutes() - (input.getTimezoneOffset()));
    }

    output = input;// dateWithTimezone;//.zone('+0100').format('HH:mm');//

    return output;
}