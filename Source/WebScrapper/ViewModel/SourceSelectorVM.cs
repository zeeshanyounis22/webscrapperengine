﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebScrapper.ViewModel
{
    public class SourceSelectorVM
    {
        public long ID { get; set; }
        [Required]
        [Display(Name = "Source Title")]
        public string SourceTitle { get; set; }
        [Required]
        [Display(Name = "Source URL")]
        public string SourceURL { get; set; }
        [Display(Name = "News Container")]
        public string NewsContainer { get; set; }
        [Display(Name = "Css Selecor")]
        public string CssSelecor { get; set; }
        [Display(Name = "Item Selector")]
        public string ItemSelector { get; set; }
        [Display(Name = "Link Selector")]
        public string LinkSelector { get; set; }
        [Display(Name = "Heading Selector")]
        public string HeadingSelector { get; set; }
        [Display(Name = "Date Selector")]
        public string DateSelector { get; set; }
        [Display(Name = "Description Selector")]
        public string DescriptionSelector { get; set; }
        [Display(Name = "DateFormat Selector")]
        public string DateFormatSelector { get; set; }
        [Display(Name = "Culture Selector")]
        public string CultureSelector { get; set; }
    }
}