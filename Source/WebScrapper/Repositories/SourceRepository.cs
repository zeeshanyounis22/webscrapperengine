﻿using Scrapper.DataAccess;
using Scrapper.Model.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebScrapper.ViewModel;

namespace WebScrapper.Repositories
{
    public class SourceRepository
    {
        ScrapperEntities _db = new ScrapperEntities();
        public List<SourceModel> GetSources()
        {
            List<SourceModel> scrapUrls = new List<SourceModel>();
            var sources = _db.Sources.ToList();
            if (sources.Count > 0)
            {
                foreach (var item in sources)
                {
                    var source = new SourceModel()
                    {
                        ID = item.Id,
                        SourceTitle = HttpUtility.HtmlDecode(item.SourceTitle),
                        SourceURL = HttpUtility.HtmlDecode(item.SourceURL),
                        ExtractedDate = item.ExtractedDate.Value
                    };
                    scrapUrls.Add(source);
                }
            }

            return scrapUrls;
        }

        public bool Add(SourceModel model)
        {
            bool isAdded = false;
            DateTime utcTime = DateTime.Now.ToUniversalTime();
            var source = new Source()
            {
                SourceTitle = model.SourceTitle,
                SourceURL = model.SourceURL,
                ExtractedDate = utcTime
            };
            _db.Sources.Add(source);
            try
            {
                _db.SaveChanges();
                isAdded = true;
            }
            catch (Exception)
            {
                throw;
            }
            return isAdded;
        }

        public void UpdateScrap(int id)
        {
            var source = _db.Sources.Where(x => x.Id == id).FirstOrDefault();
            if (source != null)
                try
                {
                    source.ExtractedDate = DateTime.UtcNow;
                    _db.Entry(source).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();
                }
                catch (Exception expe)
                {
                }

        }

        public bool DeleteSource(int id)
        {
            bool isDeleted = false;
            var source = _db.Sources.Where(x => x.Id == id).FirstOrDefault();
            if (source != null)
                try
                {
                    _db.Sources.Remove(source);
                    _db.SaveChanges();
                    isDeleted = true;
                }
                catch (Exception expe)
                {
                }
            return isDeleted;
        }

        public SourceModel GetSouceById(int id)
        {
            SourceModel sourceVM = null;
            var source = _db.Sources.Where(x => x.Id == id).FirstOrDefault();
            if (source != null)
            {
                sourceVM = new SourceModel()
                {
                    ID = source.Id,
                    SourceTitle = HttpUtility.HtmlDecode(source.SourceTitle),
                    SourceURL = HttpUtility.HtmlDecode(source.SourceURL),
                    ExtractedDate = source.ExtractedDate
                };
            }
            return sourceVM;
        }

        public bool UpdateSource(SourceModel model)
        {
            bool isUpdate = false;
            var source = _db.Sources.Where(x => x.Id == model.ID).FirstOrDefault();
            if (source != null)
                try
                {
                    source.SourceTitle = HttpUtility.HtmlDecode(model.SourceTitle);
                    source.SourceURL = HttpUtility.HtmlDecode(model.SourceURL);
                    _db.Entry(source).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();
                    isUpdate = true;
                }
                catch (Exception expe)
                {
                }
            return isUpdate;
        }

       
    }
}