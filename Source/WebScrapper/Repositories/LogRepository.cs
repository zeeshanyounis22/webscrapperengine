﻿using Scrapper.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebScrapper.Repositories
{
    public class LogRepository
    {
        ScrapperEntities _db = new ScrapperEntities();

        public List<Log> GetLogs()
        {
            return _db.Logs.ToList();
        }

        public void LogSourceInfo(Log model)
        {
            if(model != null)
            {
                _db.Logs.Add(model);
                _db.SaveChanges();
            }
        }

        public bool DeleteLog(int id)
        {
            bool isDeleted = false;
            var log = _db.Logs.Where(x => x.LogId == id).FirstOrDefault();
            if (log != null)
                try
                {
                    _db.Logs.Remove(log);
                    _db.SaveChanges();
                    isDeleted = true;
                }
                catch (Exception expe)
                {
                }
            return isDeleted;
        }
    }
}