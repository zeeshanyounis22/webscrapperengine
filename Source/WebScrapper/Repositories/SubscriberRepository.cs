﻿using Scrapper.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebScrapper.Repositories
{
    public class SubscriberRepository
    {
        ScrapperEntities _db = new ScrapperEntities();
        public List<Subscriber> GetSubscribers()
        {
            var lstSubscribers = _db.Subscribers.ToList();           

            return lstSubscribers;
        }

        public bool Add(Subscriber model)
        {
            bool isAdded = false;
            var user = HttpContext.Current.Session["User"] as User;
          
            model.UserID = user.Id;
            model.CreatedBy = user.Id;
            model.CreatedDate = DateTime.Now;
            _db.Subscribers.Add(model);
            try
            {
                _db.SaveChanges();
                isAdded = true;
            }
            catch (Exception)
            {
                throw;
            }
            return isAdded;
        }

        public bool DeleteSubscriber(int id)
        {
            bool isDeleted = false;
            var subscriber = _db.Subscribers.Where(x => x.ID == id).FirstOrDefault();
            if (subscriber != null)
                try
                {
                    _db.Subscribers.Remove(subscriber);
                    _db.SaveChanges();
                    isDeleted = true;
                }
                catch (Exception expe)
                {
                }
            return isDeleted;
        }

        public Subscriber GetSubscriberById(int id)
        {
            return _db.Subscribers.Where(x => x.ID == id).FirstOrDefault();
        }

        public bool UpdateSubscriber(Subscriber model)
        {
            bool isUpdate = false;
            var user = HttpContext.Current.Session["User"] as User;
            var subscriber = _db.Subscribers.Where(x => x.ID == model.ID).FirstOrDefault();
            if (subscriber != null)
                try
                {
                    subscriber.FirstName = model.FirstName;
                    subscriber.LastName = model.LastName;
                    subscriber.UserID = model.UserID;
                    subscriber.Daily = model.Daily;
                    subscriber.Instantly = model.Instantly;
                    subscriber.Weekly = model.Weekly;
                    subscriber.Email = model.Email;
                    subscriber.ModifiedBy = user.Id;
                    subscriber.ModifiedDate = DateTime.Now;
                    _db.Entry(subscriber).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();
                    isUpdate = true;
                }
                catch (Exception expe)
                {
                }
            return isUpdate;
        }
    }
}