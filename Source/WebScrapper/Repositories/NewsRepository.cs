﻿using Scrapper.DataAccess;
using Scrapper.Model.News;
using Scrapper.Utility.Debug;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebScrapper.Repositories
{

    public class NewsRepository
    {
        ScrapperEntities _db = new ScrapperEntities();

        public List<NewsModel> GetNews()
        {
            var lstNews = new List<NewsModel>();
            var newsList = _db.News.OrderByDescending(x=>x.NewsDate).ToList();
            if (newsList.Count > 0)
            {
                foreach (var item in newsList)
                {
                    var news = new NewsModel()
                    {
                        ID = item.Id,
                        Heading = item.Heading,
                        Description = item.Description,
                        ArticleURL = item.ArticleURL,
                        SourceTitle = item.SourceTitle,
                        SourceURL = item.SourceURL,
                        NewsDate = item.NewsDate.Value
                    };
                    lstNews.Add(news);
                }

            }
            return lstNews;
        }

        public void SaveNews(List<NewsModel> listNews)
        {
            foreach (var item in listNews)
            {
                var news = new News()
                {
                    Heading = item.Heading,
                    Description = item.Description,
                    ArticleURL = item.ArticleURL,
                    SourceTitle = item.SourceTitle,
                    SourceURL = item.SourceURL,
                    NewsDate = item.NewsDate,
                    CreatedDate = DateTime.Now
                };
                _db.News.Add(news);
                _db.SaveChanges();
            }
        }

        public void InsertExtractedNews(List<NewsModel> listNews)
        {
            foreach (var item in listNews)
            {
                if (item.NewsDate != DateTime.MinValue)
                {
                    var news = new ExtractedNew()
                    {
                        Heading = item.Heading,
                        Description = item.Description,
                        ArticleURL = item.ArticleURL,
                        SourceTitle = item.SourceTitle,
                        SourceURL = item.SourceURL,
                        NewsDate = item.NewsDate,
                        CreatedDate = DateTime.Now
                    };
                    _db.ExtractedNews.Add(news);
                    try
                    {
                        _db.SaveChanges();
                    }
                    catch (SqlException ex)
                    {
                        Logger.LogInformationWithNoQueue(ex.Message);
                    }

                }
            }
        }

        public void RunUpdateProcedure()
        {
            try
            {
                _db.Database.ExecuteSqlCommand("SP_UpdateNews");
            }
            catch (SqlException ex)
            {
                Logger.LogInformationWithNoQueue(ex.Message);
                TruncateExtractedNews();
            }
            
        }

        public void DeleteNews()
        {
            _db.Database.ExecuteSqlCommand("Delete from News");
        }

        public void TruncateExtractedNews()
        {
            _db.Database.ExecuteSqlCommand("truncate table ExtractedNews");
            // _db.Database.ExecuteSqlCommand("truncate table News");
        }

        public bool IsNewsExists(string heading)
        {
            bool isExists = false;
            isExists = _db.News.Where(x => x.Heading == heading).Any();
            return isExists;
        }

        public List<News> GetNewsByInterval()
        {
            List<News> newsList = new List<News>();
            string query = "SELECT * from News where CreatedDate > DATEADD(HOUR, -1, GETDATE())";
            newsList = _db.Database.SqlQuery<News>(query).OrderByDescending(x => x.NewsDate).ToList();

            return newsList;
        }

        public List<News> GetDailyNews()
        {
            List<News> newsList = new List<News>();
            string query = "select * from News where CreatedDate>= DATEADD(hh, -24, GETDATE())";
            newsList = _db.Database.SqlQuery<News>(query).OrderByDescending(x => x.NewsDate).ToList();

            return newsList;
        }

        public List<News> GetWeeklyNews()
        {
            List<News> newsList = new List<News>();
            string query = @"select * from News WHERE CreatedDate >= DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()) -7, 0)
                            AND CreatedDate < GETDATE()";
            newsList = _db.Database.SqlQuery<News>(query).OrderByDescending(x => x.NewsDate).ToList();

            return newsList;
        }
    }
}