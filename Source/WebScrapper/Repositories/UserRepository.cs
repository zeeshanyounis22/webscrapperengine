﻿using Scrapper.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebScrapper.ViewModel;

namespace WebScrapper.Repositories
{
    public class UserRepository
    {
        ScrapperEntities _db = new ScrapperEntities();

        public User Login(LoginViewModel user)
        {
            User userObj = null;
            userObj = _db.Users.Where(x => x.Email == user.Email && x.Password == user.Password).FirstOrDefault();
            if (userObj != null)
            {
                return userObj;
            }
            return userObj;
        }

        public bool Register(RegisterViewModel userVM)
        {
            bool isAdded = false;
            var user = new User()
            {
                FirstName = userVM.FirstName,
                LastName = userVM.LastName,
                Password = userVM.Password,
                Email = userVM.Email,
                RoleId = userVM.RoleType,
                IsActive = true,
                CreatedDate = DateTime.Now,
            };
            try
            {
                _db.Users.Add(user);
                _db.SaveChanges();
                isAdded = true;
            }
            catch (Exception)
            {
                isAdded = false;
                throw;
            }
            return isAdded;
        }

        public bool IsUserExist(string email)
        {
            bool isExists = _db.Users.Where(x => x.Email == email).Any();
            return isExists;
        }

        public List<Role> GetRoles()
        {
            var lstRoles = _db.Roles.ToList();
            return lstRoles;
        }

        public List<User> GetUsers()
        {
            var lstUsers = _db.Users.ToList();
            return lstUsers;
        }

        public bool DeleteUser(int id)
        {
            bool isDeleted = false;
            var user = _db.Users.Where(x => x.Id == id).FirstOrDefault();
            
            if (user != null)
                try
                {
                    _db.Users.Remove(user);
                    _db.SaveChanges();
                    isDeleted = true;
                }
                catch (Exception expe)
                {
                }
            return isDeleted;
        }

        public RegisterViewModel GetUserById(int id)
        {
            var user = _db.Users.Where(x => x.Id == id).FirstOrDefault();
            var roles = _db.Roles.ToList();
            var userVM = new RegisterViewModel()
            {
                ID = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Password = user.Password,
                RoleType = Convert.ToInt32(user.RoleId),
                Role = user.Role,
                Roles = roles.Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Role1,
                    Selected=true
                })
            };
            return userVM;

        }

        public bool UpdateUser(RegisterViewModel model)
        {
            bool isUpdate = false;
            var user = HttpContext.Current.Session["User"] as User;
            var _user = _db.Users.Where(x => x.Id == model.ID).FirstOrDefault();
            if (_user != null)
                try
                {
                    _user.FirstName = model.FirstName;
                    _user.LastName = model.LastName;
                    _user.Email = model.Email;
                    _user.RoleId = model.RoleType;
                    _db.Entry(_user).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();
                    isUpdate = true;
                }
                catch (Exception expe)
                {
                }
            return isUpdate;
        }

    }
}