﻿using Scrapper.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebScrapper.ViewModel;

namespace WebScrapper.Repositories
{
    public class SelectorRepository
    {
        ScrapperEntities _db = new ScrapperEntities();
        public List<SourceSelector> GetSelectors()
        {
            return _db.SourceSelectors.ToList();
        }

        public bool SaveSelector(SourceSelectorVM selector)
        {
            bool isAdded = false;
            var _selecor = new SourceSelector()
            {
                SourceTitle = selector.SourceTitle,
                SourceURL = selector.SourceURL,
                NewsContainer = selector.NewsContainer,
                ItemSelector = selector.ItemSelector,
                CssSelecor = selector.CssSelecor,
                LinkSelector = selector.LinkSelector,
                HeadingSelector = selector.HeadingSelector,
                DateSelector = selector.DateSelector,
                DescriptionSelector = selector.DescriptionSelector,
                DateFormatSelector = selector.DateFormatSelector,
                CultureSelector = selector.CultureSelector,
                 CreatedDate = DateTime.UtcNow
            };
            _db.SourceSelectors.Add(_selecor);
            try
            {
                _db.SaveChanges();
                isAdded = true;
            }
            catch (Exception)
            {
                throw;
            }
            return isAdded;
        }

        public SourceSelector GetSelectorByID(int id)
        {
            var selector = _db.SourceSelectors.Where(x => x.ID == id).FirstOrDefault();
            if(selector != null)
            {
                return selector;
            }
            return selector;
        }

        public bool UpdateSelector(SourceSelectorVM model)
        {
            bool isUpdated = false;
            var selector = _db.SourceSelectors.Where(x => x.ID == model.ID).FirstOrDefault();
            if(selector != null)
            {
                selector.NewsContainer = model.NewsContainer;
                selector.ItemSelector = model.ItemSelector;
                selector.CssSelecor = model.CssSelecor;
                selector.LinkSelector = model.LinkSelector;
                selector.HeadingSelector = model.HeadingSelector;
                selector.DateSelector = model.DateSelector;
                selector.DescriptionSelector = model.DescriptionSelector;
                selector.DateFormatSelector = model.DateFormatSelector;
                selector.CultureSelector = model.CultureSelector;
            }
            try
            {
                _db.Entry(selector).State = System.Data.Entity.EntityState.Modified;
                _db.SaveChanges();
                isUpdated = true;
            }
            catch (Exception)
            {
                throw;
            }
            return isUpdated;
        }

        public void UpdateScrap(long id)
        {
            var source = _db.SourceSelectors.Where(x => x.ID == id).FirstOrDefault();
            if (source != null)
                try
                {
                    source.ExtractedDate = DateTime.UtcNow;
                    _db.Entry(source).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();
                }
                catch (Exception expe)
                {
                }

        }

        public bool DeleteUser(int id)
        {
            bool isDeleted = false;
            var user = _db.SourceSelectors.Where(x => x.ID == id).FirstOrDefault();

            if (user != null)
                try
                {
                    _db.SourceSelectors.Remove(user);
                    _db.SaveChanges();
                    isDeleted = true;
                }
                catch (Exception expe)
                {
                }
            return isDeleted;
        }
    }
}