﻿using Hangfire;
using System.Data.Entity;
using System.Web.Hosting;


namespace WebScrapper.App_Start
{
    public class ApplicationPreload : System.Web.Hosting.IProcessHostPreloadClient
    {
        public void Preload(string[] parameters)
        {
            HangfireBootstrapper.Instance.Start();
          //  HangfireAspNet.Use(Startup.GetHangfireConfiguration);
        }
    }
}