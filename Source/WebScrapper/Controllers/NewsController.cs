﻿
using Scrapper.Utility.Scrapper;
using System.Data;
using System.Web.Mvc;
using WebScrapper.Repositories;

namespace WebScrapper.Controllers
{
    [Authorize]
    public class NewsController : Controller
    {
        NewsRepository _news = new NewsRepository();
        [Authorize]
       [AllowAnonymous]
        public ActionResult Index()
        {
          
            if (Session["User"] != null)
            {
                var lstNews = _news.GetNews();
                return View(lstNews);
            }else
            {
                return RedirectToAction("Login","User");
            }
            
        }

      

        public ActionResult ExportToExcel()
        {
            var lstNews = _news.GetNews();
            DataSet ds = Scrapper.Utility.Scrapper.Common.ToDataSet(lstNews);
            DataTable dt = ds.Tables[0];
            Scrapper.Utility.Scrapper.Common.WriteExcel(dt, "xls");
            return View();
        }


       
    }
}