﻿
using Scrapper.DataAccess;
using Scrapper.Model.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebScrapper.Repositories;

namespace WebScrapper.Controllers
{
    [Authorize]
    public class SubscriberController : Controller
    {
        // GET: Subscriber
        SubscriberRepository _db = new SubscriberRepository();
        public ActionResult Index()
        {
            var lstSubscribers = _db.GetSubscribers();
            return View(lstSubscribers);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Subscriber model)
        {
            if (ModelState.IsValid)
            {
                bool isCreated = _db.Add(model);
                if (isCreated)
                    return RedirectToAction("Index");
                else
                    return View();
            }
            else
                return View();
        }

        //[ChildActionOnly]
        public ActionResult Delete(int id)
        {
            bool isDeleted = _db.DeleteSubscriber(id);
            if (isDeleted)
                return RedirectToAction("Index");
            else
                return View();
        }

        public ActionResult Edit(int id)
        {
            var source = _db.GetSubscriberById(id);
            return View(source);
        }

        [HttpPost]
        public ActionResult Edit(Subscriber model)
        {
            bool isUpdated = _db.UpdateSubscriber(model);
            if (isUpdated)
            {
                return RedirectToAction("Index");
            }
            else
                return View();
        }
    }
}