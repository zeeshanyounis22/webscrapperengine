﻿using Newtonsoft.Json;
using PagedList;
using Scrapper.Model.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebScrapper.Repositories;

namespace WebScrapper.Controllers
{
    [Authorize]
    public class Source2Controller : Controller
    {
        // GET: Source
        SourceRepository _rep = new SourceRepository();
        LogRepository _log = new LogRepository();
        public ActionResult Index(int? page)
        {
            if (User.Identity.IsAuthenticated && Session["User"] != null)
            {
                var sources = _rep.GetSources();
                String serializedResult = JsonConvert.SerializeObject(sources);
                ViewBag.Sources = serializedResult;

                int pageSize = 5;
                int pageNumber = (page ?? 1);
                return View(sources.ToPagedList(pageNumber, pageSize));
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(SourceModel model)
        {
            if (ModelState.IsValid)
            {
                bool isCreated = _rep.Add(model);
                if (isCreated)
                    return RedirectToAction("Index");
                else
                    return View();
            }
            else
                return View();
        }

        //[ChildActionOnly]
        public ActionResult Delete(int id)
        {
            bool isDeleted = _rep.DeleteSource(id);
            if (isDeleted)
                return RedirectToAction("Index");
            else
                return View();
        }

        public ActionResult Edit(int id)
        {
            var source = _rep.GetSouceById(id);
            return View(source);
        }

        [HttpPost]
        public ActionResult Edit(SourceModel model)
        {
            bool isUpdated = _rep.UpdateSource(model);
            if(isUpdated)
            {
                return RedirectToAction("Index");
            }
            else
            return View();
        }

        public ActionResult ViewLog()
        {
            var logList = _log.GetLogs();
            String serializedResult = JsonConvert.SerializeObject(logList);
            ViewBag.Logs = serializedResult;
            return View(logList);
        }

        public JsonResult ViewLogData()
        {
            var logList = _log.GetLogs();
            var serializedResult = JsonConvert.SerializeObject(logList);
            var jsonData = new
            {
                data = logList
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteLog(int id)
        {
            bool isDeleted = _log.DeleteLog(id);
            if (isDeleted)
                return RedirectToAction("ViewLog");
            else
                return View();
        }
    }
}