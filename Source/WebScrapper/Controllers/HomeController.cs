﻿using System.Web.Mvc;
using WebScrapper.Models;
using WebScrapper.Repositories;

namespace WebScrapper.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        SourceRepository _rep = new SourceRepository();
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated && Session["User"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }

        [AllowAnonymous]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Error()
        {
            var message = new ErrorModel()
            {
                PageTitle = "UnAuthorize",
                Message = "Access Denied",
                Description = "Sorry, you don't have access to that page."
            };
            return View(message);
        }
    }
}