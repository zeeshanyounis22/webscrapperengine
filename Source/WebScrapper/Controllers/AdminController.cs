﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebScrapper.Repositories;
using WebScrapper.ViewModel;

namespace WebScrapper.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        SelectorRepository _rep = new SelectorRepository();
        // GET: Admin

        public ActionResult Index()
        {
            var lstSelectors = _rep.GetSelectors();
            return View(lstSelectors);
        }

        public ActionResult CreateSelecor()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateSelecor(SourceSelectorVM selector)
        {
            if(ModelState.IsValid)
            {
                var isNewAdded = _rep.SaveSelector(selector);
                if (isNewAdded)
                {
                    ViewBag.Message = "New Selector Added successfully";
                    ModelState.Clear();
                }
            }          
            return View();
        }

        public ActionResult Edit(int id)
        {
            var source = _rep.GetSelectorByID(id);
            return View(source);
        }

        [HttpPost]
        public ActionResult Edit(SourceSelectorVM model)
        {
            bool isUpdated = _rep.UpdateSelector(model);
            if (isUpdated)
            {
                return RedirectToAction("Index");
            }
            else
                return View();
        }

        public ActionResult Delete(int id)
        {
            bool isDeleted = _rep.DeleteUser(id);
            if (isDeleted)
                return RedirectToAction("Index");
            else
                return View();
        }
    }
}