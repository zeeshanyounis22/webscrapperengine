﻿using PagedList;
using Scrapper.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebScrapper.Repositories;
using WebScrapper.ViewModel;

namespace WebScrapper.Controllers
{
    public class UserController : Controller
    {
        UserRepository _rep = new UserRepository();
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _rep.Login(model);
                if (user != null)
                {
                    Session["User"] = user;
                    FormsAuthentication.SetAuthCookie(user.Id.ToString(), true);
                    return RedirectToAction("Index", "News");
                }

            }
            return View();
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            var roleslist = _rep.GetRoles();
            ViewBag.Roles = roleslist;
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool isUserExists = _rep.IsUserExist(model.Email);
                if (!isUserExists)
                {
                    bool isRegistered = _rep.Register(model);
                    if (isRegistered)
                    {
                        ViewBag.Message = "User created Successfully";
                        ModelState.Clear();
                    }
                }
                else
                {
                    ViewBag.Message = "User with this " + model.Email + " already registered";
                }

            }
            var roleslist = _rep.GetRoles();
            ViewBag.Roles = roleslist;
            ModelState.Clear();
            return View(new RegisterViewModel());
        }


        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login", "User");
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                //var user = await UserManager.FindByNameAsync(model.Email);
                //if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                //{
                //    // Don't reveal that the user does not exist or is not confirmed
                //    return View("ForgotPasswordConfirmation");
                //}
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ViewResult UsersList(string sortOrder, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.FirstNameSortParm = String.IsNullOrEmpty(sortOrder) ? "firstname_desc" : "";
            ViewBag.LastNameSortParm = String.IsNullOrEmpty(sortOrder) ? "lastname_desc" : "";

            var users = _rep.GetUsers();
          
            switch (sortOrder)
            {
                case "firstname_desc":
                    users = users.OrderByDescending(s => s.FirstName).ToList();
                    break;
                case "FirstName":
                    users = users.OrderBy(s => s.FirstName).ToList();
                    break;
                case "lastname_desc":
                    users = users.OrderByDescending(s => s.LastName).ToList();
                    break;
                case "LastName":
                    users = users.OrderBy(s => s.LastName).ToList();
                    break;
                case "role_desc":
                    users = users.OrderByDescending(s => s.LastName).ToList();
                    break;
                default:  // Name ascending 
                    users = users.OrderBy(s => s.FirstName).ToList();
                    break;
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(users.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Delete(int id)
        {
            bool isDeleted = _rep.DeleteUser(id);
            if (isDeleted)
                return RedirectToAction("UsersList");
            else
                return View();
        }

        public ActionResult Edit(int id)
        {
            var source = _rep.GetUserById(id);
            return View(source);
        }

        [HttpPost]
        public ActionResult Edit(RegisterViewModel model)
        {
            bool isUpdated = _rep.UpdateUser(model);
            if (isUpdated)
            {
                return RedirectToAction("UsersList");
            }
            else
                return View();
        }

    }
}