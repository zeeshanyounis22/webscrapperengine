﻿using HtmlAgilityPack;
using Quartz;
using Scrapper.DataAccess;
using Scrapper.Model.Email;
using Scrapper.Utility.Debug;
using Scrapper.Utility.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using WebScrapper.Repositories;

namespace WebScrapper.Jobs
{
    public class SendEmailJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            Logger.LogInformationWithNoQueue($"===================== Process Start ==================== " + DateTime.Now);
            string jobType = context.JobDetail.Key.Name;
            Logger.LogInformationWithNoQueue($"===================== Email Type  {jobType} ==================== ");
            
            Send(jobType);
        }

        public void Send(string jobType)
        {
            #region variables
            SubscriberRepository _db = new SubscriberRepository();
            Logger.LogInformationWithNoQueue($"===================== Getting Subscribers List from database ==================== ");
            var subscribers = _db.GetSubscribers();
            Logger.LogInformationWithNoQueue($"===================== Subscribers Count :  {subscribers.Count} ==================== ");
            string email = string.Empty;
            NewsRepository _news = new NewsRepository();
            List<News> lstNews = null;
            string emailBody = string.Empty;
            string heading = string.Empty;

            #endregion

            #region getting job based on jobType
            if (jobType == "instantly")
            {
                lstNews = _news.GetNewsByInterval();
                Logger.LogInformationWithNoQueue($"===================== News Count :  {lstNews.Count} ==================== ");
                heading = ConfigurationManager.AppSettings["Instantly"].ToString();
                email = string.Join(",", subscribers.Where(x => x.Instantly == true).Select(x => x.Email).ToList());
            }
            if (jobType == "daily")
            {
                lstNews = _news.GetDailyNews();
                Logger.LogInformationWithNoQueue($"===================== News Count :  {lstNews.Count} ==================== ");
                heading = ConfigurationManager.AppSettings["Daily"].ToString();
                email = string.Join(",", subscribers.Where(x => x.Daily == true).Select(x => x.Email).ToList());
            }
            if (jobType == "weekly")
            {
                lstNews = _news.GetWeeklyNews();
                Logger.LogInformationWithNoQueue($"===================== News Count :  {lstNews.Count} ==================== ");
                heading = ConfigurationManager.AppSettings["Weekly"].ToString();
                email = string.Join(",", subscribers.Where(x => x.Weekly == true).Select(x => x.Email).ToList());
            }
            #endregion

            Logger.LogInformationWithNoQueue($"===================== Sending Email ==================== " + DateTime.Now);
            #region Sending Email
            if (lstNews != null)
            {
                emailBody = MakeBody(lstNews, heading);
            }
            else
            {
                emailBody = "No New News found";
            }

            EmailMessageModel emailmodel = new EmailMessageModel
            {
                To = email,
                Subject = heading,
                Body = emailBody
            };
            if (jobType == "instantly" && lstNews.Count != 0)
            {
                SendNewsItems(emailmodel, email);
            }
            if ((jobType == "daily" && lstNews.Count != 0))
            {
                SendNewsItems(emailmodel, email);
            }
            else if (jobType == "weekly")
            {
                SendNewsItems(emailmodel, email);
            }

            #endregion Sending Email
            
            Logger.LogInformationWithNoQueue($"===================== Email Sending Process Completed ==================== " + DateTime.Now);
        }

        public void SendNewsItems(EmailMessageModel emailmodel, string email)
        {
            if (!string.IsNullOrEmpty(email))
            {
                var address = emailmodel.To.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in address)
                {
                    emailmodel.To = item;
                    EmailHelper.Send(emailmodel);
                    ClearTemplate();
                }
            }
        }

        /// <summary>
        /// Create body for email using Hrml template from folder
        /// </summary>
        /// <param name="News List"></param>
        /// <returns>Html string</returns>
        private static string MakeBody(List<News> lstNewsHeadings, string headline)
        {
            StringBuilder sb = new StringBuilder();
            HtmlDocument document = new HtmlDocument();
            string filePath = HttpRuntime.AppDomainAppPath + "\\EmailTemplate\\News.html";
            WebClient client = new WebClient();

            document.Load(filePath);

            var body = document.GetElementbyId("lstNews");
            var heading = document.GetElementbyId("headline");
            heading.InnerHtml = headline;
            string webURL = ConfigurationManager.AppSettings["WebURL"].ToString();
            var anchor = document.GetElementbyId("webUrl");
            anchor.Attributes["href"].Value = webURL;
            //header of table
            #region Table Header
            sb.Append("<tr style='font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px;'>");
            sb.Append(" <td class='table-row-td' style='width: 10%;padding-right: 16px; padding-bottom: 12px; font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;' valign='top' align='left'>");
            sb.Append("<span style='font-family: Arial, sans-serif; line-height: 19px; color: #000; font-size: 13px; font-weight: bold;'>");
            sb.Append("News Date");
            sb.Append("</span>");
            sb.Append("</td>");
            sb.Append("<td class='table-row-td' style='width: 25%;padding-bottom: 12px; font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;' valign='top' align='left'>");
            sb.Append("<span style='font-family: Arial, sans-serif; line-height: 19px; color: #000; font-size: 13px; font-weight: bold;'>");
            sb.Append("Source URL");
            sb.Append("</span>");
            sb.Append("</td>");
            sb.Append("<td class='table-row-td' style='width: 35%;padding-bottom: 12px; font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;' valign='top' align='left'>");
            sb.Append("<span style='font-family: Arial, sans-serif; line-height: 19px; color: #000; font-size: 13px; font-weight: bold;'>");
            sb.Append("Heading");
            sb.Append("</span>");
            sb.Append("</td>");
            #endregion
            #region Make table rows for news
            foreach (var news in lstNewsHeadings)
            {
                //get Day Name from date
                DateTime dt = Convert.ToDateTime(news.NewsDate);
                var dow = dt.DayOfWeek;
                string strDay = dow.ToString();

                //check wither http protocol is present or not
                if (news.ArticleURL != null)
                {
                    if (news.ArticleURL.Contains("http") || news.ArticleURL.Contains("https"))
                    {
                        news.ArticleURL = news.ArticleURL;
                    }
                    else
                    {
                        news.ArticleURL = "http://" + news.ArticleURL;
                    }
                }
                else
                {
                    news.ArticleURL = news.SourceURL;
                }

                sb.Append("<tr style='font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px;'>");
                sb.Append("<td class='table-row-td' style='width: 10%;padding-right: 16px; padding-bottom: 12px; font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;' valign='top' align='left'>");
                sb.Append("<span style='font-family: Arial, sans-serif; line-height: 19px; color: #478fca; font-size: 13px; font-weight: bold;'>");
                sb.Append(strDay);
                sb.Append("</span><br>");
                sb.Append(dt.ToString("dd-MM-yyyy"));
                sb.Append("</td>");

                #region Source title
                sb.Append("<td class='table-row-td' style='width: 25%;padding-bottom: 12px; font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;' valign='top' align='left'>");
                sb.Append("<b>");
                if (news.SourceURL != "")
                {
                    sb.Append("<a href='" + news.SourceURL + "' style='color:#428bca;text-decoration: none;'>");
                }
                sb.Append(news.SourceTitle);
                sb.Append("</a></b>");
                #endregion

                sb.Append("<td class='table-row-td' style='width: 35%;padding-bottom: 12px; font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;' valign='top' align='left'>");
                sb.Append("<b>");
                if (news.Description != "")
                {
                    sb.Append("<a href='" + news.ArticleURL + "' title='" + news.Description + "' style='color:#428bca;text-decoration: none;'>");
                }
                else
                {
                    sb.Append("<a href='" + news.ArticleURL + "' title='No Description Found' style='color:#428bca'>");
                }
                sb.Append(news.Heading);
                sb.Append("</a></b>");
                sb.Append("</td>");
                sb.Append("</tr>");

            }
            #endregion
            ClearTemplate();
            body.InnerHtml = sb.ToString();
            document.Save(filePath);

            //getting updated html after adding news list
            string htmlString = document.DocumentNode.OuterHtml;
            return htmlString;
        }

        private static void ClearTemplate()
        {
            string filePath = HttpRuntime.AppDomainAppPath + "\\EmailTemplate\\News.html";
            HtmlDocument document = new HtmlDocument();
            document.Load(filePath);

            var body = document.GetElementbyId("lstNews");
            body.InnerHtml = "";
            document.Save(filePath);
        }
    }
}