﻿using Quartz;
using Scrapper.Utility.Debug;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

namespace WebScrapper.Jobs
{
    public class AwakingWebsiteJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            string webURL = ConfigurationManager.AppSettings["WebURL"];
            try
            {
                Logger.LogInformationWithNoQueue($"===================== Awaking Website ==================== " + DateTime.Now);
                var url = webURL + "?timestamp=" + DateTime.Now.Ticks;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AllowAutoRedirect = false;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                response.Close();
            }
            catch (Exception ex)
            {
                Logger.LogInformationWithNoQueue($"===================== {ex.Message} ==================== ");
            }
           
        }
    }
}