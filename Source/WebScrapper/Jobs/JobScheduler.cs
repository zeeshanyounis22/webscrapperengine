﻿using Quartz;
using Quartz.Impl;
using Quartz.Impl.Triggers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WebScrapper.Jobs
{
    public class JobScheduler
    {
        private static IScheduler _scheduler;
        public static void Start()
        {
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            _scheduler = schedulerFactory.GetScheduler();
            _scheduler.Start();
            ScheduleJobs();
        }

        private static void ScheduleJobs()
        {
            string webPooling = ConfigurationManager.AppSettings["WebPoolingCron"].ToString();
            string instantly = ConfigurationManager.AppSettings["InstantlyCron"].ToString();
            string daily = ConfigurationManager.AppSettings["DailyCron"].ToString();
            string weekly = ConfigurationManager.AppSettings["WeeklyCron"].ToString();

            ScheduleJob(new ScrapingJob(),
                new JobDetails
                {
                    JobName = "instantly",
                    JobGroup = "Group1",
                    TriggerName = "TriggerInstantly",
                    TriggerGroup = "Group1",
                    TriggerExpression = instantly
                });

            ScheduleJob(new AwakingWebsiteJob(),
               new JobDetails
               {
                   JobName = "awake",
                   JobGroup = "Group5",
                   TriggerName = "TriggerAwaking",
                   TriggerGroup = "Group5",
                   TriggerExpression = webPooling
               });

            var emailJobSchedules = new List<JobDetails>{
                new JobDetails
                {
                    JobName = "instantly",
                    JobGroup = "Group2",
                    TriggerName = "Trigger1",
                    TriggerGroup = "Group2",
                    TriggerExpression = "0 0 0/1 1/1 * ? *"
                },
                 new JobDetails
                {
                    JobName = "daily",
                    JobGroup = "Group3",
                    TriggerName = "TriggerDaily",
                    TriggerGroup = "Group3",
                    TriggerExpression = daily
                },
                new JobDetails
                {
                    JobName = "weekly",
                    JobGroup = "Group4",
                    TriggerName = "TriggerWeekly",
                    TriggerGroup = "Group4",
                    TriggerExpression = weekly
                }
            };

            foreach(var jobDetail in emailJobSchedules)
            {
                ScheduleJob(new SendEmailJob(), jobDetail);
            }          


            //IJob sendDailyEmail = new SendEmailJob(); //This Constructor needs to be parameterless
            //JobDetailImpl sendDailyJobDetial = new JobDetailImpl("daily", "Group3", sendDailyEmail.GetType());
            //CronTriggerImpl dailyEmailTrigger = new CronTriggerImpl("Trigger1", "Group3", daily);
            //_scheduler.ScheduleJob(sendDailyJobDetial, dailyEmailTrigger);

            //IJob sendWeeklyEmail = new ScrapingJob(); //This Constructor needs to be parameterless
            //JobDetailImpl sendWeeklyJobDetial = new JobDetailImpl("weekly", "Group4", sendWeeklyEmail.GetType());
            //CronTriggerImpl weeklyEmailTrigger = new CronTriggerImpl("Trigger2", "Group4", weekly);
            //_scheduler.ScheduleJob(sendWeeklyJobDetial, weeklyEmailTrigger);
        }

        private static DateTimeOffset ScheduleJob(IJob job, JobDetails jobDetails)
        {
            JobDetailImpl jobDetialImp = new JobDetailImpl(jobDetails.JobName, jobDetails.JobGroup, job.GetType());
            CronTriggerImpl jobTriggerImp = new CronTriggerImpl(jobDetails.TriggerName, jobDetails.TriggerGroup, jobDetails.TriggerExpression);
            return _scheduler.ScheduleJob(jobDetialImp, jobTriggerImp);
        }

        public class JobDetails
        {
            public string JobName { get; set; }
            public string JobGroup { get; set; }
            public string TriggerName { get; set; }
            public string TriggerGroup { get; set; }
            public string TriggerExpression { get; set; }
        }
    }
}