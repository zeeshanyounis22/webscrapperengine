﻿using Scrapper.Utility.Debug;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Scrapper.Model;
using WebScrapper.Repositories;
using ScrapingEngine.IO;
using ScrapingEngine.Logic;
using Scrapper.DataAccess;
using System.Diagnostics;
using Hangfire;
using Scrapper.Model.News;

namespace WebScrapper.Jobs
{
    public class JobTask
    {
        #region global variables
        SelectorRepository _rep = new SelectorRepository();
        NewsRepository _news = new NewsRepository();
        LogRepository _log = new LogRepository();
        SendMail email = new SendMail();
        List<NewsModel> lstAll = null;
        bool isExit = false;
        StringBuilder ErrorLog;
        #endregion
        
        public void Execute()
        {
            _news.TruncateExtractedNews();

            lstAll = new List<NewsModel>();
            ErrorLog = new StringBuilder();
            //grtting list of source URLs
            var selectors = _rep.GetSelectors();
            List<NewsModel> newsItems = null;
            Logger.LogInformationWithNoQueue($"================================== Start Process =================================");

            foreach (var selector in selectors)
            {
                if (!string.IsNullOrEmpty(selector.SourceURL))
                {
                    isExit = false;
                    var watch = Stopwatch.StartNew();
                    var startTime = DateTime.UtcNow.TimeOfDay;
                    newsItems = new List<NewsModel>();
                    
                    Logger.LogInformationWithNoQueue($"Scraping {selector.SourceURL} ....");

                    Logger.LogInformationWithNoQueue($"Downloading html contents ....");
                    var content = NewsReader.ReadNewsFrom(selector.SourceURL);
                    Logger.LogInformationWithNoQueue($"Html download completed ....");

                    var newsHtml = NewsFinder.FindNews(content, selector);

                    Logger.LogInformationWithNoQueue($"Parsing news items ....");
                    newsItems = NewsParser.ParseNews(newsHtml, selector, isExit);
                    lstAll.AddRange(newsItems);
                    _rep.UpdateScrap(selector.ID);
                    Logger.LogInformationWithNoQueue($"Scraping {selector.SourceURL} ....Done");
                    Logger.LogInformationWithNoQueue($"Found === {newsItems.Count} Items");
                                    
                    SaveLog(selector, newsItems, watch, startTime);
                    watch.Stop();
                }
            }

            if (lstAll.Count > 0)
            {
               // _news.SaveNews(lstAll);
                _news.RunUpdateProcedure();
                _news.TruncateExtractedNews();
            }

            Logger.LogInformationWithNoQueue($"================================== Process Completed =================================");
            //  email.Send(jobType);
        }

        public void SaveLog(SourceSelector selector, List<NewsModel> newsItems, Stopwatch watch, TimeSpan startTime)
        {
            var endTime = DateTime.UtcNow.TimeOfDay;
            var timeDuration = watch.Elapsed.TotalSeconds;
            var log = new Log()
            {
                SourceURL = selector.SourceURL,
                StartTime = startTime,
                EndTime = endTime,
                ExtractedItems = newsItems.Count,
                CreatedOn = DateTime.UtcNow
            };
            if (newsItems.Count > 0)
            {
                _news.InsertExtractedNews(newsItems);
                _log.LogSourceInfo(log);
            }
        }
    }
}