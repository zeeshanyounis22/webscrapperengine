﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace WebScrapper.Jobs
{
    public class ScrapingJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            JobTask job = new JobTask();
            job.Execute();
        }
    }
}