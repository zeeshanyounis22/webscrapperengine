﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebScrapper.Models
{
    public class ErrorModel
    {
        public string PageTitle { get; set; }
        public string Message { get; set; }
        public string Description { get; set; }
    }
}