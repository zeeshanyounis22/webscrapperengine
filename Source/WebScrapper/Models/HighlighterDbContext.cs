﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebScrapper.Models
{
    public class HighlighterDbContext : DbContext
    {
        public HighlighterDbContext()
           : base("HighlighterDb")
        {
        }
    }
}