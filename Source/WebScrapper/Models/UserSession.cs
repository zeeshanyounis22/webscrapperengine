﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebScrapper.Repositories;

namespace WebScrapper.Models
{
    public class UserSession
    {
        UserRepository _db = new UserRepository();

        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }


    }
}