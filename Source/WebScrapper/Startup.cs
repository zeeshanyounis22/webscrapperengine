﻿using System;
using System.Collections.Generic;
using Hangfire.Dashboard;

using Microsoft.Owin;
using Owin;
using Hangfire;
using WebScrapper.Jobs;
using WebScrapper;
using WebScrapper.Filters;
using WebScrapper.Attributes;
using System.Configuration;

[assembly: OwinStartup(typeof(Startup))]
namespace WebScrapper
{
    public partial class Startup
    {
        string daily = ConfigurationManager.AppSettings["DailyCron"].ToString();
        string weekly = ConfigurationManager.AppSettings["WeeklyCron"].ToString();
        public static IEnumerable<IDisposable> GetHangfireConfiguration()
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage("HighlighterDb");
            GlobalJobFilters.Filters.Add(new LogEverythingAttribute());
            GlobalConfiguration.Configuration.UseFilter(new JobContext());
            yield return new BackgroundJobServer();
        }
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            //app.UseHangfireAspNet(GetHangfireConfiguration);
           
            //app.UseHangfireDashboard("/hangfire", new DashboardOptions
            //{
            //    Authorization = new[] { new HangFireAuthorizationFilter() },
               
            //});

            //RecurringJob.AddOrUpdate<JobTask>("NewsJob.GetNews", x => x.Excute("instantly"), Cron.Hourly);
            //RecurringJob.AddOrUpdate<JobTask>("NewsJob.GetNewsByEveryDay", x => x.Excute("daily"),daily, TimeZoneInfo.Local);
            //RecurringJob.AddOrUpdate<JobTask>("NewsJob.GetNewsByEveryWeek", x => x.Excute("weekly"), weekly, TimeZoneInfo.Local);
        }
    }
}
