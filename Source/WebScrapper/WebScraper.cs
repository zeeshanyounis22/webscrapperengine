﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebScrapper.IO;
using WebScrapper.Logic;
using WebScrapper.Model;

namespace WebScrapper
{
    public static class WebScraper
    {
        public static List<News> ScrapNews()
        {
            List<News> newsList = new List<News>();
            var selectors = NewsReader.GetSelectors();
            List<News> newsItems = null;
            var heading = $"================================== Start Process =================================";
            Console.Out.WriteLine(heading);
            Console.WriteLine();
            foreach (var selector in selectors.Skip(1))
            {
                if(!string.IsNullOrEmpty(selector.URL))
                {
                    newsItems = new List<News>();
                    Console.Write($"Scraping {selector.URL} ....");
                    var content = NewsReader.ReadNewsFrom(selector.URL);
                    var newsHtml = NewsFinder.FindNews(content, selector);
                    newsItems = NewsParser.ParseNews(newsHtml, selector);
                    newsList.AddRange(newsItems);
                    Console.Write(" Done ");
                    Console.WriteLine();
                }                
            }
            return newsList;
        }
    }
}
