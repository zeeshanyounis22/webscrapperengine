﻿using System;
using System.Threading;
using WebScrapper.IO;

namespace WebScrapper
{
    static class Program
    {
       
        static void Main(string[] args)
        {
           //Extracting news list from source file
            var newsList = WebScraper.ScrapNews();
            var endMessage = $"================================== Extraction Completed =================================";
            Console.Out.WriteLine(endMessage);
            Console.WriteLine();
            Thread.Sleep(5000);

            var writingExcel = $"================================== Saving data to Excel File  =================================";
            Console.Out.WriteLine(writingExcel);
            //write news list to excel file
            NewsWriter.ExportToExcel(newsList);
            Thread.Sleep(3000);
            var writingExcelCompleted = $"================================== Completed  =================================";
            Console.Out.WriteLine(writingExcelCompleted);
        }
    }
}
