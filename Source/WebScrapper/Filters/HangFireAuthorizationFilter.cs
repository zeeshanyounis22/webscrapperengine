﻿using Hangfire.Dashboard;
using Scrapper.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangfire.Annotations;
using WebScrapper.Models;
using System.Web.Mvc;
using Microsoft.Owin;
using System.Security.Policy;

namespace WebScrapper.Filters
{
    public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            bool boolAuthorizeCurrentUserToAccessHangFireDashboard = true;
            //if (HttpContext.Current.Session["User"] != null)
            //{
            //    var user = HttpContext.Current.Session["User"];
            //    if (HttpContext.Current.User.Identity.IsAuthenticated && user != null)
            //    {
            //        var u = user as User;

            //        //if (HttpContext.Current.User.IsInRole("Account Administrator"))
            //        if (u.RoleId == 1)
            //        {
            //            boolAuthorizeCurrentUserToAccessHangFireDashboard = true;
            //        }
            //        else
            //        {
            //            HttpContext.Current.Response.Redirect("Home/Error");
            //        }

            //    }
            //}
            return boolAuthorizeCurrentUserToAccessHangFireDashboard;
        }

        //public bool Authorize(IDictionary<string, object> owinEnvironment)
        //{
        //    bool boolAuthorizeCurrentUserToAccessHangFireDashboard = false;

        //    var user = HttpContext.Current.Session["User"] as User;
        //    if (HttpContext.Current.User.Identity.IsAuthenticated && user != null)
        //    {
        //        //if (HttpContext.Current.User.IsInRole("Account Administrator"))
        //        if (user.RoleId == 1)
        //            boolAuthorizeCurrentUserToAccessHangFireDashboard = true;
        //    }

        //    return boolAuthorizeCurrentUserToAccessHangFireDashboard;
        //}
    }
}