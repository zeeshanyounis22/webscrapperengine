﻿using Hangfire.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebScrapper.Filters
{
    public class JobContext : IServerFilter
    {
        [ThreadStatic]
        private static string _jobId;
        [ThreadStatic]
        private static Type _jobType;

        public static string JobId { get { return _jobId; } set { _jobId = value; } }

        public static Type JobType { get { return _jobType; } set { _jobType = value; } }

        public void OnPerforming(PerformingContext context)
        {
            JobId = context.JobId;
            JobType = context.Job.Type;
        }

        public void OnPerformed(PerformedContext filterContext)
        {
        }
    }
}