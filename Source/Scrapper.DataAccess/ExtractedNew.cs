//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Scrapper.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExtractedNew
    {
        public int Id { get; set; }
        public string SourceTitle { get; set; }
        public string SourceURL { get; set; }
        public string Heading { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> NewsDate { get; set; }
        public string ArticleURL { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<bool> IsUpdated { get; set; }
    }
}
