﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrapper.Model.News
{
    public class SourceModel
    {
        public int ID { get; set; }
        [Required]
        public string SourceTitle { get; set; }
        [Required]
        public string SourceURL { get; set; }
        public DateTime? ExtractedDate { get; set; }
    }
}
