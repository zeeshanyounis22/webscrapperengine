﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrapper.Model.News
{
    public class NewsModel
    {
        public int ID { get; set; }
        public string Heading { get; set; }
        public string Description { get; set; }
        public DateTime NewsDate { get; set; }
        public string DateString { get; set; }
        public string ArticleURL { get; set; }
        public string SourceTitle { get; set; }
        public string SourceURL { get; set; }
        public bool IsLatest
        {
            get
            {
                DateTime date;
                if (DateTime.TryParseExact(DateString, "dd-MM-yyyy", CultureInfo.InvariantCulture,
                   DateTimeStyles.AdjustToUniversal, out date) == false && date == DateTime.MinValue)
                {
                    DateTime.TryParse(DateString, out date);
                    if (date.Date == DateTime.Now.Date)
                        return true;
                }
                //if (DateTime.TryParse(DateString, out date))
                //{
                //    if (date.Date == DateTime.Now.Date)
                //        return true;
                //}

                return false;
            }
        }

        public bool HasValidDate
        {
            get
            {
                DateTime date;

                return DateTime.TryParse(DateString, out date);
            }
        }
    }
}
