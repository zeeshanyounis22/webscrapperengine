﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrapper.Model.Extensions
{
    static class Extensions
    {
        public static DataSet ToDataSet<T>(this IList<T> list)
        {
            Type elementType = typeof(T);
            DataSet ds = new DataSet();
            DataTable t = new DataTable();
            ds.Tables.Add(t);

            //add a column to table for each public property on T
            foreach (var propInfo in elementType.GetProperties())
            {
                Type ColType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

                t.Columns.Add(propInfo.Name, ColType);
            }

            //go through each property on T and add each value to the table
            foreach (T item in list)
            {
                DataRow row = t.NewRow();

                foreach (var propInfo in elementType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                }

                t.Rows.Add(row);
            }

            return ds;
        }

        public static DateTime ToDateTime(this string datetime, char dateSpliter, char dotSpliter)
        {
            try
            {
                int year = 0; int month = 0; int day = 0;
                string[] date = null;
                datetime = datetime.Trim();
                datetime = datetime.Replace("  ", " ");
                string[] body = datetime.Split(' ');
                date = body[0].Split(dateSpliter);
                if (date.Length > 0 && datetime.ToString().Contains(dateSpliter))
                {
                    year = Convert.ToInt32(date[2]);
                    month = Convert.ToInt32(date[1]);
                    day = Convert.ToInt32(date[0]);
                }
                date = body[0].Split(dotSpliter);
                if (date.Length > 0 && datetime.ToString().Contains(dotSpliter))
                {
                    year = Convert.ToInt32(date[2]);
                    month = Convert.ToInt32(date[1]);
                    day = Convert.ToInt32(date[0]);
                }
                return new DateTime(year, month, day);
            }
            catch
            {
                return new DateTime();
            }
        }
    }
}
