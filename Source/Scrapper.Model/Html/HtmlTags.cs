﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrapper.Model.Html
{
    public class HtmlTagModel
    {
        public string TagName { get; set; }
    }
}
