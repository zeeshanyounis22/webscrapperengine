﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ScrapingEngine.Logic;
using System.Web;
using NPOI.HSSF.UserModel;
using System.Net;
using HtmlAgilityPack;
using System.Globalization;

namespace ScrapingEngine.Util
{
    public static class Common
    {
        private const string DateRegEx =
          @"(?i-x)\b(?:(?:3[01]|[12][0-9]|0?[1-9])[\t ]+(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+[0-9]{4}|(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+(?:3[01]|[12][0-9]|0?[1-9]),[\t ]+[0-9]{4}|(?:3[01]|[12][0-9]|0?[1-9])\.[\t ]+(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+[0-9]{4}|(?:3[01]|[12][0-9]|0?[1-9])[/.-](?:1[0-2]|0?[1-9])[/.-][0-9]{4}|(?:1[0-2]|0?[1-9])[/.-](?:3[01]|[12][0-9]|0?[1-9])[/.-][0-9]{4}|[0-9]{4}[/.-](?:1[0-2]|0?[1-9])[/.-](?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])-(?:1[0-2]|0?[1-9])-[0-9]{4}|(?:1[0-2]|0?[1-9])-(?:3[01]|[12][0-9]|0?[1-9])-[0-9]{4}|[0-9]{4}-(?:1[0-2]|0?[1-9])-(?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])\.(?:1[0-2]|0?[1-9])\.[0-9]{4}|(?:1[0-2]|0?[1-9])\.(?:3[01]|[12][0-9]|0?[1-9])\.[0-9]{4}|[0-9]{4}\.(?:1[0-2]|0?[1-9])\.(?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])(?:3[01]|[12][0-9]|0?[1-9])-(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)-[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2})\b";
      
        public static DateTime? CleanDate(string newsDate, string dateFormat, string culture)
        {
            var dt = DateTime.MinValue;

            try
            {
                // Instantiate the regular expression object.
                Regex r = new Regex(DateRegEx, RegexOptions.IgnoreCase);

                // Match the regular expression pattern against a text string.
                Match m = r.Match(newsDate);

                if (m.Success)
                {
                    DateParser.Parse(m.Value, dateFormat, culture, out dt);
                }
                else
                {
                    dt = DateParser.IsFormattedDate(newsDate, dateFormat);
                }
                if (dt == DateTime.MinValue)
                {
                    dt = DateParser.IsFormattedDate(newsDate, dateFormat);
                }
            }
            catch (ArgumentException ex)
            {
                // Syntax error in the regular expression
            }

            return dt == DateTime.MinValue ? (DateTime?)null : dt;
        }

        public static bool IsLatestDate(DateTime dateString)
        {
            bool isLatest = true;

            DateTime givenDate = DateTime.Now;

            DateTime todayDate = DateTime.Now;
            if (todayDate.ToShortDateString() == dateString.ToShortDateString())
            {
                isLatest = true;
            }
            else
            {
                isLatest = false;
            }

            return isLatest;
        }
        public static DataSet ToDataSet<T>(this IList<T> list)
        {
            Type elementType = typeof(T);
            DataSet ds = new DataSet();
            DataTable t = new DataTable();
            ds.Tables.Add(t);

            //add a column to table for each public property on T
            foreach (var propInfo in elementType.GetProperties())
            {
                Type ColType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

                t.Columns.Add(propInfo.Name, ColType);
            }

            //go through each property on T and add each value to the table
            foreach (T item in list)
            {
                DataRow row = t.NewRow();

                foreach (var propInfo in elementType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                }

                t.Rows.Add(row);
            }

            return ds;
        }

        public static void WriteExcel(DataTable dt, String extension)
        {

            IWorkbook workbook;

            if (extension == "xlsx")
            {
                workbook = new XSSFWorkbook();
            }
            else if (extension == "xls")
            {
                workbook = new HSSFWorkbook();
            }
            else
            {
                throw new Exception("This format is not supported");
            }

            ISheet sheet1 = workbook.CreateSheet("Sheet 1");

            //make a header row
            IRow row1 = sheet1.CreateRow(0);

            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row1.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(columnName);
            }

            //loops through data
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                IRow row = sheet1.CreateRow(i + 1);
                for (int j = 0; j < dt.Columns.Count; j++)
                {

                    ICell cell = row.CreateCell(j);
                    String columnName = dt.Columns[j].ToString();
                    string columnValue = dt.Rows[i][columnName].ToString();
                    if (columnName == "DateString")
                    {
                        DateTime dateTo = Convert.ToDateTime(columnValue);
                        string strDate = dateTo.ToString("dd-MM-yyyy");
                        cell.SetCellValue(strDate);
                    }
                    else
                    {
                        cell.SetCellValue(columnValue);
                    }
                }
            }

            #region Save data to excel
            string filePath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Output\\NewsList.xlsx";
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            using (var file = new FileStream(filePath, FileMode.CreateNew, FileAccess.ReadWrite))
            {
                try
                {
                    workbook.Write(file);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    file.Dispose();
                }
            }

            #endregion
        }

        public static string EncodeString(string selector, HtmlDocument doc)
        {
            string text = string.Empty;
            if (selector == "//a")
            {
                if (doc.DocumentNode.SelectNodes(selector).FirstOrDefault().Attributes["href"] != null)
                {
                    text = doc.DocumentNode.SelectNodes(selector).FirstOrDefault().InnerText;
                }
            }
            else
            {
                text = doc.DocumentNode.SelectNodes(selector).FirstOrDefault().InnerText;
            }
            
            return WebUtility.HtmlDecode(text);
        }

        public static string GetArticleURL(string selector, HtmlDocument doc)
        {
            string url = string.Empty;
            if (doc.DocumentNode.SelectNodes(selector).FirstOrDefault().Attributes["href"] != null)
            {
                url = doc.DocumentNode.SelectNodes(selector).FirstOrDefault().Attributes["href"].Value;
            }
            return url;
        }

        public static DateTime ToDateTime(this string datetime, char dateSpliter, char dotSpliter)
        {
            try
            {
                int year = 0; int month = 0; int day = 0;
                string[] date = null;
                if(datetime.Contains(":"))
                {
                    datetime = datetime.Split(':')[0];
                }
                datetime = datetime.Trim();
                datetime = datetime.Replace("  ", " ");
                string[] body = datetime.Split(' ');
                date = body[0].Split(dateSpliter);
                if (date.Length > 0 && datetime.ToString().Contains(dateSpliter))
                {
                    year = Convert.ToInt32(date[2]);
                    month = Convert.ToInt32(date[1]);
                    day = Convert.ToInt32(date[0]);
                }
                date = body[0].Split(dotSpliter);
                if (date.Length > 1 && date[1].ToString() == "")
                {
                    DateTime dd = DateTime.Parse(datetime);
                    return dd;
                }
                else
                {
                    if (date.Length > 0 && datetime.ToString().Contains(dotSpliter))
                    {
                        year = Convert.ToInt32(date[2]);
                        month = Convert.ToInt32(date[1]);
                        day = Convert.ToInt32(date[0]);
                    }
                    return new DateTime(year, month, day);
                }
            }
            catch
            {
                return new DateTime();
            }
        }

        public static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^0-9_.-]+", "", RegexOptions.Compiled);
        }
    }
}
