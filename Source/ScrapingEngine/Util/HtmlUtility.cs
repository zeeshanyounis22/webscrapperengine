﻿
using CsQuery;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using ScrapingEngine.IO;
using ScrapingEngine.Logic;
using ScrapingEngine.Model;
using Scrapper.DataAccess;
using Scrapper.Utility.Debug;

namespace ScrapingEngine.Util
{
    public static class HtmlUtility
    {
        public static List<HtmlNode> GetHtmlByAgilityPack(string html, SourceSelector selector)
        {
            HtmlNodeCollection collections = null;
            var nodes = new List<HtmlNode>();
            var doc = new HtmlDocument();
            try
            {
                doc.LoadHtml(html);

                collections = doc.DocumentNode.SelectNodes(selector.NewsContainer);
                if (collections.Count > 0)
                {
                    if (string.IsNullOrEmpty(selector.ItemSelector))
                    {
                        nodes = FindNewsDomObjects(collections, selector, html);
                    }
                   if (nodes.Count == 0)
                    {
                        foreach (var item in collections)
                        {
                            var newsNodesDoc = new HtmlDocument();
                            newsNodesDoc.LoadHtml(item.OuterHtml);
                            var newsNodes = newsNodesDoc.DocumentNode.SelectNodes(selector.ItemSelector);
                            if (newsNodes != null)
                            {
                                foreach (var news in newsNodes)
                                {
                                    nodes.Add(news);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogInformationWithNoQueue($"Function Name GetHtmlByAgilityPack  Exception {ex.Message} ....");
            }
            return nodes;
        }

        public static List<HtmlNode> GetHtmlByAgilityPackCss(string html, SourceSelector selector)
        {
            var content = NewsReader.ReadNewsFrom(selector.SourceURL);
            var doc = new HtmlDocument();
            doc.LoadHtml(content);
            IList<HtmlNode> cssNodes = doc.QuerySelectorAll(selector.CssSelecor);
            var nodes = new List<HtmlNode>();

            if (nodes != null)
            {
                foreach (var news in cssNodes)
                {
                    nodes.Add(news);
                }
            }
            return nodes;
        }

        public static List<HtmlNode> FindNewsDomObjects(HtmlNodeCollection nodes, SourceSelector selector, string html)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(html);
            var nodesList = new List<HtmlNode>();
            HtmlNode dateDiv=null, headingDiv=null, descDiv=null;

            foreach (var item in nodes.Where(x => x.InnerText != ""))
            {
                DateTime? date= null;
                var isNewContainer = NewNodeBegins(item, selector, out date);

                if(isNewContainer)
                {
                    var divContainer = CreateNode(doc, "div", "container");

                    dateDiv = CreateNode(doc, "div", "date");
                    dateDiv.InnerHtml = date.Value.ToShortDateString();
                    headingDiv = CreateNode(doc, "div", "heading");
                    descDiv = CreateNode(doc, "div", "description");

                    divContainer.AppendChild(dateDiv);
                    divContainer.AppendChild(headingDiv);
                    divContainer.AppendChild(descDiv);

                    nodesList.Add(divContainer);
                }
                else
                {
                    var isHeading = IsNewsHeading(item);

                    if (isHeading)
                        headingDiv?.AppendChild(item);
                    else
                        descDiv?.AppendChild(item);
                }

            }

            return nodesList;
        }

        private static bool IsNewsHeading(HtmlNode item)
        {
            if (item.HasChildNodes && item.ChildNodes.Where(i => i.Name == "strong" || item.Name == "strong").Any())
                return true;
            return false;
        }

        private static HtmlNode CreateNode(HtmlDocument doc, string elementName, string cssClass)
        {
            var node = doc.CreateElement(elementName);
            node.Attributes.Add("class", cssClass);
            return node;
        }

        private static bool NewNodeBegins(HtmlNode item, SourceSelector selector, out DateTime? date)
        {
            var text = WebUtility.HtmlDecode(item.InnerText);
            if (item.InnerText.Contains("København"))
            {
                text = item.InnerText.Trim().Replace("København", "2017");
            }
            if (item.InnerText.Contains("Allinge") )
            {
                text = item.InnerText.Trim().Replace("Allinge", "");
            }
            date = Common.CleanDate(text, selector.DateFormatSelector, selector.CultureSelector);
            return date.HasValue;
        }
    }
}
