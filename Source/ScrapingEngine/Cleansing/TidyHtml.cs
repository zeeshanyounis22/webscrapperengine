﻿using TidyManaged;
using ScrapingEngine.Model;

namespace ScrapingEngine.Cleansing
{
    public class TidyHtml:IHtmlCleaner
    {
        public string CleanHtml(string html)
        {
            string output;

            using (var doc = Document.FromString(html))
            {
                doc.ShowWarnings = false;
                doc.Quiet = true;
                doc.OutputXhtml = true;
                doc.CleanAndRepair();
                output = doc.Save();
            }

            return output;
        }
    }
}