﻿using NReadability;
using ScrapingEngine.Model;

namespace ScrapingEngine.Cleansing
{
    public class ReadableHtml:IHtmlCleaner
    {
        public string CleanHtml(string html)
        {
            var transcoder = new NReadabilityTranscoder();
            var input = new TranscodingInput(html);
            var output = transcoder.Transcode(input);

            return output.ExtractedContent;
        }
    }
}
