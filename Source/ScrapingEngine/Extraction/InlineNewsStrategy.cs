﻿using HtmlAgilityPack;
using System.Collections.Generic;
using ScrapingEngine.Model;
using Scrapper.DataAccess;

namespace ScrapingEngine.Extraction
{
    /// <summary>
    /// This strategy finds news items listed as one inline list with
    /// all the news contained in shared container as in http://www.medicinraadet.dk/nyheder
    /// </summary>
    public class InlineNewsStrategy: IFindNewsStrategy
    {
        public IEnumerable<HtmlNode> FindNewsDomObjects(string html, SourceSelector selector)
        {
            return null;
        }
    }
}
