﻿using System.Collections.Generic;
using System.Linq;
using CsQuery;
using ScrapingEngine.Model;
using HtmlAgilityPack;
using ScrapingEngine.Util;
using Scrapper.DataAccess;

namespace ScrapingEngine.Extraction
{
    public class PatternSearchStrategy: IFindNewsStrategy
    {
        static readonly List<string> Patterns = new List<string>
            {
                @"div:visible > article:visible",
                @"div:visible > section:visible > article:visible",
                @"div:visible > ul:visible > li:visible"
            };
        public IEnumerable<HtmlNode> FindNewsDomObjects(string html, SourceSelector selectors)
        {
            var cqHtml = CQ.Create(html);

            var nodes = new List<HtmlNode>();
            nodes = HtmlUtility.GetHtmlByAgilityPack(html, selectors);
        
            return nodes;
        }
    }
}
