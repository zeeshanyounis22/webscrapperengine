using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;
using ScrapingEngine.Extraction;
using ScrapingEngine.Model;
using Scrapper.DataAccess;

namespace ScrapingEngine.Logic
{
   public static class NewsFinder
    {
        public static IEnumerable<HtmlNode> FindNews(string html, SourceSelector selectors)
        {
            var searchStrategies = new IFindNewsStrategy[]
            {
                new PatternSearchStrategy(),
                //new InlineNewsStrategy(),
                //new DataMiningStrategy()
            };

            var newNodes = new List<HtmlNode>();

            foreach (var strategy in searchStrategies)
            {
                var result = strategy.FindNewsDomObjects(html, selectors);
                if(result != null && result.Count() > 0)
                {
                    var news = result as HtmlNode[] ?? result.ToArray();

                    //If this strategy does not found the news then continue
                    if (!news.Any()) continue;

                    newNodes.AddRange(news);
                    break;
                }
              
            }

            return newNodes;
        }
    }
}