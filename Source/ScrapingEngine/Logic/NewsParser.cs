﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using CsQuery;
using HtmlAgilityPack;
using ScrapingEngine.Util;
using Scrapper.DataAccess;
using Scrapper.Utility.Debug;
using Scrapper.Model.News;

namespace ScrapingEngine.Logic
{
    public static class NewsParser
    {
        private const string DateRegEx =
            @"(?i-x)\b(?:(?:3[01]|[12][0-9]|0?[1-9])[\t ]+(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+[0-9]{4}|(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+(?:3[01]|[12][0-9]|0?[1-9]),[\t ]+[0-9]{4}|(?:3[01]|[12][0-9]|0?[1-9])\.[\t ]+(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+[0-9]{4}|(?:3[01]|[12][0-9]|0?[1-9])[/.-](?:1[0-2]|0?[1-9])[/.-][0-9]{4}|(?:1[0-2]|0?[1-9])[/.-](?:3[01]|[12][0-9]|0?[1-9])[/.-][0-9]{4}|[0-9]{4}[/.-](?:1[0-2]|0?[1-9])[/.-](?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])-(?:1[0-2]|0?[1-9])-[0-9]{4}|(?:1[0-2]|0?[1-9])-(?:3[01]|[12][0-9]|0?[1-9])-[0-9]{4}|[0-9]{4}-(?:1[0-2]|0?[1-9])-(?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])\.(?:1[0-2]|0?[1-9])\.[0-9]{4}|(?:1[0-2]|0?[1-9])\.(?:3[01]|[12][0-9]|0?[1-9])\.[0-9]{4}|[0-9]{4}\.(?:1[0-2]|0?[1-9])\.(?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])(?:3[01]|[12][0-9]|0?[1-9])-(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)-[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2})\b";
        //  @"\b(?:(?:3[01]|[12][0-9]|0?[1-9])[\t ]+(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+[0-9]{4}|(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+(?:3[01]|[12][0-9]|0?[1-9]),[\t ]+[0-9]{4}|(?:3[01]|[12][0-9]|0?[1-9])\.[\t ]+(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+[0-9]{4}|(?:3[01]|[12][0-9]|0?[1-9])[/.-](?:1[0-2]|0?[1-9])[/.-][0-9]{4}|(?:1[0-2]|0?[1-9])[/.-](?:3[01]|[12][0-9]|0?[1-9])[/.-][0-9]{4}|[0-9]{4}[/.-](?:1[0-2]|0?[1-9])[/.-](?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])-(?:1[0-2]|0?[1-9])-[0-9]{4}|(?:1[0-2]|0?[1-9])-(?:3[01]|[12][0-9]|0?[1-9])-[0-9]{4}|[0-9]{4}-(?:1[0-2]|0?[1-9])-(?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])\.(?:1[0-2]|0?[1-9])\.[0-9]{4}|(?:1[0-2]|0?[1-9])\.(?:3[01]|[12][0-9]|0?[1-9])\.[0-9]{4}|[0-9]{4}\.(?:1[0-2]|0?[1-9])\.(?:3[01]|[12][0-9]|0?[1-9]))\b";

        private static readonly string[] DateMarkers = {
            "Jan", "January", "januar", "Feb", "Febr", "February", "februar", "Mar", "March", "marts", "Apr", "April",
            "May", "May", "Maj", "Jun", "June", "Juni", "Jul", "July", "Juli", "Aug", "August", "Sep", "Sept",
            "September", "Oct", "Okt", "October", "Oktober", "Nov", "November", "Dec", "December", "2016", "2017",
            "2018", "2019", "2020"
        };
        public static NewsModel TryParseNewsNode(HtmlNode newsText, SourceSelector selector, out bool parsed)
        {
            NewsModel news = null;
            if (newsText != null)
            {
                news = new NewsModel();
                var newsNodesDoc = new HtmlDocument();
                newsNodesDoc.LoadHtml(newsText.OuterHtml);
                try
                {
                    if (newsNodesDoc.DocumentNode.SelectNodes(selector.HeadingSelector) != null)
                    {
                        news.Heading = Common.EncodeString(selector.HeadingSelector, newsNodesDoc);
                    }
                    if (!string.IsNullOrEmpty(selector.LinkSelector) && newsNodesDoc.DocumentNode.SelectNodes(selector.LinkSelector) != null)
                    {
                        news.ArticleURL = Common.GetArticleURL(selector.LinkSelector, newsNodesDoc);
                    }
                    if (newsNodesDoc.DocumentNode.SelectNodes(selector.DateSelector) != null)
                    {
                        news.DateString = newsNodesDoc.DocumentNode.SelectNodes(selector.DateSelector).FirstOrDefault().InnerText;
                    }
                    if (!string.IsNullOrEmpty(selector.DescriptionSelector) && newsNodesDoc.DocumentNode.SelectNodes(selector.DescriptionSelector) != null)
                    {
                        news.Description = Common.EncodeString(selector.DescriptionSelector, newsNodesDoc);
                    }
                }
                catch (HtmlWebException ex)
                {

                    Logger.LogInformationWithNoQueue(ex.Message);
                }
            }
            news.SourceTitle = selector.SourceTitle;

            Uri result;
            if (Uri.TryCreate(news.ArticleURL, UriKind.Absolute, out result) == false)
                news.ArticleURL = (new Uri(new Uri(selector.SourceURL), news.ArticleURL)).OriginalString;

            var date = Common.CleanDate(news.DateString, selector.DateFormatSelector, selector.CultureSelector);
            
            //news.DateString = date?.ToString("dd-MM-yyyy") ?? string.Empty;
           
            if (!string.IsNullOrEmpty(news.DateString) && news.DateString.Contains("-"))
            {
                string ss = Common.RemoveSpecialCharacters(news.DateString);
                news.NewsDate = Common.ToDateTime(ss, '-','.');
                if(news.NewsDate == DateTime.MinValue)
                {
                    news.NewsDate = date.Value;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(news.DateString))
                {
                    news.DateString = date?.ToShortDateString() ?? string.Empty;
                    news.NewsDate = date ?? DateTime.MinValue;
                }
              
            }
            news.SourceURL = selector.SourceURL;
            try
            {
                parsed = !string.IsNullOrEmpty(news.Heading) &&
                    !string.IsNullOrEmpty(news.ArticleURL) &&
                    !string.IsNullOrEmpty(news.DateString);

            }
            catch (Exception ex)
            {
                parsed = false;
                Logger.LogInformationWithNoQueue(ex.Message);
            }


            return news;
        }

        public static List<NewsModel> ParseNews(IEnumerable<HtmlNode> newsNodes, SourceSelector selector, bool isExit)
        {
            var newsList = new List<NewsModel>();

            foreach (var newsNode in newsNodes)
            {
                if (isExit)
                {
                    break;
                }
                else
                {
                    var parsed = false;

                    var news = TryParseNewsNode(newsNode, selector, out parsed);

                    if (!string.IsNullOrEmpty(news.DateString))
                    {
                        if (parsed && Common.IsLatestDate(news.NewsDate))
                        {
                            newsList.Add(news);
                        }
                        else
                        {
                            isExit = true;
                        }
                    }
                }
            }

            return newsList;
        }

        private static bool ContainsDateMarkers(IDomObject o)
        {
            var contains = DateMarkers.Any(dateMarker => o.InnerText.Contains(dateMarker));
            return contains;
        }
    }
}