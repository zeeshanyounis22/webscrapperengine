﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace ScrapingEngine.Logic
{
    static class DateParser
    {
        public static void Parse(string dateString, string dateFormat, string culture, out DateTime date)
        {
            //TODO: Try to parse the dateString with all possible formats

            if (DateTime.TryParseExact(dateString, dateFormat, CultureInfo.InvariantCulture,
                    DateTimeStyles.AdjustToUniversal, out date) == false && date == DateTime.MinValue)
            {
                DateTime.TryParse(dateString, out date);
            }
            //if (DateTime.TryParse(dateString, out date) == false && date == DateTime.MinValue)
            //{
            //    DateTime.TryParseExact(dateString, dateFormat, CultureInfo.InvariantCulture,
            //        DateTimeStyles.AdjustToUniversal, out date);
            //}
        }

        public static DateTime IsFormattedDate(string strDate, string format)
        {
            string datString = string.Empty;
            DateTime dt = DateTime.MinValue;
            DateTime date = DateTime.MinValue;
            CultureInfo ci = new CultureInfo("da");
            string[] formats = format.Split('|');
            strDate = strDate.Trim().Replace("|", "").Replace("\r\n", " ");
            try
            {
                if (DateTime.TryParseExact(strDate.Trim(), "dd. MMMM yyyy", ci, DateTimeStyles.None, out date))
                {
                    dt = date;
                }
                if (dt == DateTime.MinValue)
                {
                    if (DateTime.TryParseExact(strDate.Trim(), "d. MMMM, yyyy", ci, DateTimeStyles.None, out date))
                    {
                        dt = date;
                    }
                }
                if (dt == DateTime.MinValue)
                {
                    foreach (var item in formats)
                    {
                        if (DateTime.TryParseExact(strDate.TrimStart().TrimEnd(), item, ci, DateTimeStyles.None, out date))
                        {
                            dt = date;
                        }
                    }
                   
                }
                return dt;
            }
            catch (Exception)
            {
            }
            return dt;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_.-|]+", "", RegexOptions.Compiled);
        }

    }
}
