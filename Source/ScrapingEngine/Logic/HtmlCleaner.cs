﻿using ScrapingEngine.Cleansing;
using ScrapingEngine.Model;

namespace ScrapingEngine.Logic
{
    public static class HtmlCleaner
    {
        public static string CleanHtml(string html)
        {
            var cleanStrategies = new IHtmlCleaner[]
            {
                new TidyHtml(), 
                //new ReadableHtml()
            };

            foreach (var strategy in cleanStrategies)
            {
                html = strategy.CleanHtml(html);
            }

            return html;
        }
    }
}
