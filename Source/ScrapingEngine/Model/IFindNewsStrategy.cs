﻿using HtmlAgilityPack;
using Scrapper.DataAccess;
using System.Collections.Generic;

namespace ScrapingEngine.Model
{
    interface IFindNewsStrategy
    {
        IEnumerable<HtmlNode> FindNewsDomObjects(string html, SourceSelector selector);
    }
}
