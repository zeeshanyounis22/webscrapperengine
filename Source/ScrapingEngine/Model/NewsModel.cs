﻿using System;
using System.Text;

namespace ScrapingEngine.Model
{
    public class NewsModel
    {
        public int ID { get; set; }
        public string Heading { get; set; }
        public string Description { get; set; }
        public string DateString { get; set; }        
        public string ArticleURL { get; set; }
        public string SourceTitle { get; set; }
        public string SourceURL { get; set; }
        public bool IsLatest
        {
            get
            {
                DateTime date;

                if (DateTime.TryParse(DateString, out date))
                {
                    if (date.Date == DateTime.Now.Date)
                        return true;
                }

                return false;
            }
        }

        public bool HasValidDate
        {
            get
            {
                DateTime date;

                return DateTime.TryParse(DateString, out date);
            }
        }

        public override string ToString()
        {
            var newsText = new StringBuilder();

            newsText.AppendLine($"Heading: { Heading }");
            newsText.AppendLine($"DateTime: { DateString }");
            //newsText.AppendLine($"HasValidDate: { HasValidDate }");
            //newsText.AppendLine($"IsLatest: { IsLatest }");
            newsText.AppendLine($"Description: { Description }");
            newsText.AppendLine($"Link: { ArticleURL }");
            newsText.AppendLine("===================================================================");

            return newsText.ToString();
        }

        public string ToHtml()
        {
            var newsText = new StringBuilder();

            newsText.AppendLine($"<div><a href='{ ArticleURL }'> { Heading } </a>");
            newsText.AppendLine($"<div>DateTime: { DateString }</div>");
            //newsText.AppendLine($"<div>HasValidDate: { HasValidDate }</div>");
            //newsText.AppendLine($"<div>IsLatest: { IsLatest }</div>");
            newsText.AppendLine($"<div>Description: { Description }</div>");
            newsText.AppendLine($"</div>");
            newsText.AppendLine("===================================================================");

            return newsText.ToString();
        }
    }
}