﻿namespace ScrapingEngine.Model
{
    interface IHtmlCleaner
    {
        string CleanHtml(string html);
    }
}
