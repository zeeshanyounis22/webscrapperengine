﻿using Excel;
using FileHelpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using ScrapingEngine.Model;
using Scrapper.DataAccess;
using Scrapper.Utility.Debug;

namespace ScrapingEngine.IO
{
    public class NewsReader
    {
        public static string ReadNewsFrom(string url)
        {
            string htmlString = string.Empty;
            var client = new WebClient { Encoding = Encoding.UTF8 };
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                htmlString = client.DownloadString(url);
            }
            catch (Exception ex)
            {
                Logger.LogInformationWithNoQueue(ex.Message);               
            }
            
            return htmlString;
        }
        public static SourceSelector GetSelectors(string url)
        {
            var engine = new FileHelperEngine<SourceSelector>();
            SourceSelector model = null;
            string filePath = System.AppDomain.CurrentDomain.BaseDirectory + "\\selectors.xlsx";
            // To Read Use:
            //  var result = engine.ReadFile(filePath);
            FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            //...
            //3. DataSet - The result of each spreadsheet will be created in the result.Tables
            DataSet result = excelReader.AsDataSet();
            //...
            //4. DataSet - Create column names from first row
            excelReader.IsFirstRowAsColumnNames = false;

            //5. Data Reader methods
            while (excelReader.Read())
            {
                foreach (DataRow row in result.Tables["Sheet1"].Rows)
                {
                    if (row[0].ToString() == url)
                    {
                        model = new SourceSelector()
                        {
                            SourceURL = row[0].ToString(),
                            NewsContainer = row[1].ToString(),
                            CssSelecor = row[2].ToString(),
                            ItemSelector = row[3].ToString(),
                            LinkSelector = row[4].ToString(),
                            HeadingSelector = row[5].ToString(),
                            DateSelector = row[6].ToString(),
                            DescriptionSelector = row[7].ToString(),
                            DateFormatSelector = row[8].ToString(),
                            CultureSelector = row[9].ToString(),
                        };
                    }
                }
            }
            return model;
        }

        public static List<SourceSelector> GetSelectors()
        {
            var engine = new FileHelperEngine<SourceSelector>();
            SourceSelector model = null;
            var lstSelectors = new List<SourceSelector>();
            string filePath = System.AppDomain.CurrentDomain.BaseDirectory + "\\selectors.xlsx";
            // To Read Use:
            //  var result = engine.ReadFile(filePath);
            FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            //...
            //3. DataSet - The result of each spreadsheet will be created in the result.Tables
            DataSet result = excelReader.AsDataSet();
            //...
            //4. DataSet - Create column names from first row
            excelReader.IsFirstRowAsColumnNames = false;

            //5. Data Reader methods

            foreach (DataRow row in result.Tables["Sheet1"].Rows)
            {
                model = new SourceSelector()
                {
                    SourceURL = row[0].ToString(),
                    NewsContainer = row[1].ToString(),
                    CssSelecor = row[2].ToString(),
                    ItemSelector = row[3].ToString(),
                    LinkSelector = row[4].ToString(),
                    HeadingSelector = row[5].ToString(),
                    DateSelector = row[6].ToString(),
                    DescriptionSelector = row[7].ToString(),
                    DateFormatSelector = row[8].ToString(),
                    CultureSelector = row[9].ToString(),
                };
                lstSelectors.Add(model);
            }

            return lstSelectors;
        }
    }
}
