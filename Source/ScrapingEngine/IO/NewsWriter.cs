﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using ScrapingEngine.Model;
using ScrapingEngine.Util;

namespace ScrapingEngine.IO
{
    public static class NewsWriter
    {
        public static void WriteToFile(string path, string content)
        {
            File.WriteAllText(path, content);
        }

        public static void WriteToFile(string path, string pageTitle, IList<NewsModel> newsList)
        {
            if (newsList == null || newsList.Count == 0)
                return;

            var newsText = new StringBuilder();

            foreach (var news in newsList)
            {
                newsText.AppendLine($"<div> {pageTitle} </div>");
                
                newsText.Append(news.ToHtml()); ;
            }

            File.WriteAllText(path, newsText.ToString());
        }

        public static void WriteToConsole(string pageTitle, IList<NewsModel> newsList)
        {
            if (newsList == null || newsList.Count == 0)
            {
                Console.Out.Write("Sorry! we were not able to find any news");
                return;
            }

            foreach (var news in newsList)
            {
                Output(pageTitle, news);
            }
        }

        private static void Output(string pageTitle, NewsModel news)
        {
            var heading = $"================================== {pageTitle} =================================";

            Console.Out.WriteLine(heading);
            Console.Out.WriteLine(news);
        }

        public static void ExportToExcel(IList<NewsModel> newsList)
        {
            DataSet ds = Common.ToDataSet(newsList);
            DataTable dt = ds.Tables[0];
            Common.WriteExcel(dt, "xlsx");
        }


    }
}
