﻿using Scrapper.Model.News;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Windows.Forms;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Scrapper.Utility.Scrapper
{
    public static class Common
    {
        public static bool CheckDate(string dateString, bool isShowLatest)
        {
            bool isLatest = true;

            DateTime givenDate = DateTime.Now;
            if (dateString.Contains("-"))
            {
                givenDate = ToDateTime(dateString, '-', 'd');
            }
            else if (dateString.Contains("."))
            {
                givenDate = ToDateTime(dateString, 'd', '.');
            }

            //DateTime todayDate = DateTime.Now;
            //if (todayDate.ToShortDateString() == givenDate.ToShortDateString())
            //{
            //    isLatest = true;
            //}
            //else
            //{
            //    isLatest = false;
            //}

            return isLatest;
        }

        public static DataSet ToDataSet<T>(this IList<T> list)
        {
            Type elementType = typeof(T);
            DataSet ds = new DataSet();
            DataTable t = new DataTable();
            ds.Tables.Add(t);

            //add a column to table for each public property on T
            foreach (var propInfo in elementType.GetProperties())
            {
                Type ColType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

                t.Columns.Add(propInfo.Name, ColType);
            }

            //go through each property on T and add each value to the table
            foreach (T item in list)
            {
                DataRow row = t.NewRow();

                foreach (var propInfo in elementType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                }

                t.Rows.Add(row);
            }

            return ds;
        }

        public static DateTime ToDateTime(this string datetime, char dateSpliter, char dotSpliter)
        {
            try
            {
                int year = 0; int month = 0; int day = 0;
                string[] date = null;
                datetime = datetime.Trim();
                datetime = datetime.Replace("  ", " ");
                string[] body = datetime.Split(' ');
                date = body[0].Split(dateSpliter);
                if (date.Length > 0 && datetime.ToString().Contains(dateSpliter))
                {
                    year = Convert.ToInt32(date[2]);
                    month = Convert.ToInt32(date[1]);
                    day = Convert.ToInt32(date[0]);
                }
                date = body[0].Split(dotSpliter);
                if (date.Length > 1 && date[1].ToString() == "")
                {
                    DateTime dd = DateTime.Parse(datetime);
                    return dd;
                }
                else
                {
                    if (date.Length > 0 && datetime.ToString().Contains(dotSpliter))
                    {
                        year = Convert.ToInt32(date[2]);
                        month = Convert.ToInt32(date[1]);
                        day = Convert.ToInt32(date[0]);
                    }
                    return new DateTime(year, month, day);
                }
            }
            catch
            {
                return new DateTime();
            }
        }

        private static void CreateExcel(List<NewsModel> lstNews)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.FileName = "Sample.xlsx";
            saveFileDialog1.Filter = "Excel 2007 files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.OverwritePrompt = false;

            //  If the user hit Cancel, then abort!
            if (saveFileDialog1.ShowDialog() != DialogResult.OK)
                return;

            string TargetFilename = saveFileDialog1.FileName;
            //  Step 1: Create a DataSet, and put some sample data in it
            DataSet ds = ToDataSet(lstNews);

            //  Step 2: Create the Excel file
            try
            {
                CreateExcelFile.CreateExcelDocument(ds, TargetFilename);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Couldn't create Excel file.\r\nException: " + ex.Message);
                return;
            }

            //  Step 3:  Let's open our new Excel file and shut down this application.
            Process p = new Process();
            p.StartInfo = new ProcessStartInfo(TargetFilename);
            p.Start();

            //this.Close();
        }

        public static HtmlAgilityPack.HtmlDocument DownloadHTML(string url)
        {
            HtmlAgilityPack.HtmlDocument html = new HtmlAgilityPack.HtmlDocument();
            html.LoadHtml(new WebClient().DownloadString(url));
            return html;
        }

        public static DateTime IsDate(string strDdate)
        {
            DateTime dt = DateTime.MinValue;
            string date = GetDate(strDdate);
            CultureInfo ci = new CultureInfo("da");
            if (!string.IsNullOrEmpty(date))
            {

                if (DateTime.TryParseExact(date, "dd-MM-yyyy",
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None,
                    out dt))
                {
                    return dt;
                }
            }
            if (dt == DateTime.MinValue)
            {
                bool isDate = DateTime.TryParse(strDdate, out dt);
                if (isDate)
                {
                    return dt;
                }
                else if (DateTime.TryParseExact(date, "dd. MMMM yyyy", ci, DateTimeStyles.None, out dt))
                {
                    return dt;
                }
                else if (DateTime.TryParseExact(date, "d. MMMM yyyy", ci, DateTimeStyles.None, out dt))
                {
                    return dt;
                }
            }

            return dt;
        }

        public static string SplitDate(string strDate)
        {
            string date = GetDate(strDate);
            //string[] dateArray = null;
            //if (strDate.Contains(":"))
            //{
            //    dateArray = strDate.Split(':');
            //}
            //else if (strDate.Contains("-"))
            //{
            //    dateArray = strDate.Split('-');
            //}
            //string date = string.Empty;
            //if (dateArray != null)
            //{
            //    date = dateArray[0].ToString();
            //}
            return date;
        }

        public static void WriteExcel(DataTable dt, String extension)
        {

            IWorkbook workbook;

            if (extension == "xlsx")
            {
                workbook = new XSSFWorkbook();
            }
            else if (extension == "xls")
            {
                workbook = new HSSFWorkbook();
            }
            else
            {
                throw new Exception("This format is not supported");
            }

            ISheet sheet1 = workbook.CreateSheet("Sheet 1");

            //make a header row
            IRow row1 = sheet1.CreateRow(0);

            for (int j = 0; j < dt.Columns.Count; j++)
            {

                ICell cell = row1.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(columnName);
            }

            //loops through data
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                IRow row = sheet1.CreateRow(i + 1);
                for (int j = 0; j < dt.Columns.Count; j++)
                {

                    ICell cell = row.CreateCell(j);
                    String columnName = dt.Columns[j].ToString();
                    string columnValue = dt.Rows[i][columnName].ToString();
                    if (columnName == "NewsDate")
                    {
                        DateTime dateTo = Convert.ToDateTime(columnValue);
                        string strDate = dateTo.ToString("dd-MM-yyyy");
                        cell.SetCellValue(strDate);
                    }
                    else
                    {
                        cell.SetCellValue(columnValue);
                    }

                }
            }

            using (var exportData = new MemoryStream())
            {
                HttpContext.Current.Response.Clear();
                workbook.Write(exportData);
                if (extension == "xlsx") //xlsx file format
                {
                    HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", "News.xlsx"));
                    HttpContext.Current.Response.BinaryWrite(exportData.ToArray());
                }
                else if (extension == "xls")  //xls file format
                {
                    HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", "News.xls"));
                    HttpContext.Current.Response.BinaryWrite(exportData.GetBuffer());
                }
                HttpContext.Current.Response.End();
            }
            #region Save data to excel
            //  string filePath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Output\\News.xlsx";
            //string filePath = HttpRuntime.AppDomainAppPath + "\\EmailTemplate\\News.xlsx";
            //if (File.Exists(filePath))
            //{
            //    File.Delete(filePath);
            //}
            //using (var file = new FileStream(filePath, FileMode.CreateNew, FileAccess.ReadWrite))
            //{
            //    try
            //    {
            //        workbook.Write(file);
            //    }
            //    catch (Exception)
            //    {
            //        throw;
            //    }
            //    finally
            //    {
            //        file.Dispose();
            //    }
            //}

            #endregion
        }

        public static bool ParseTimeSpan(string intervalStr)
        {
            // Write the first part of the output line.
            Console.Write("{0,20}   ", intervalStr);
            bool isValid = false;
            // Parse the parameter, and then convert it back to a string.
            TimeSpan intervalVal;
            if (TimeSpan.TryParse(intervalStr, out intervalVal))
            {
                string intervalToStr = intervalVal.ToString();

                // Pad the end of the TimeSpan string with spaces if it 
                // does not contain milliseconds.
                int pIndex = intervalToStr.IndexOf(':');
                pIndex = intervalToStr.IndexOf('.', pIndex);
                if (pIndex < 0)
                    intervalToStr += "        ";
                isValid = true;
                Console.WriteLine("{0,21}", intervalToStr);
                // Handle failure of TryParse method.
            }
            else
            {
                isValid = false;
                Console.WriteLine("Parse operation failed.");
            }
            return isValid;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_.-]+", "", RegexOptions.Compiled);
        }

        public static string IsFormattedDate(string strDate)
        {
            string datString = string.Empty;
            DateTime dt = DateTime.MinValue;
            CultureInfo ci = new CultureInfo("da");
            try
            {
                if (DateTime.TryParseExact(strDate.Trim(), "dd. MMMM yyyy", ci, DateTimeStyles.None, out dt))
                {
                    datString = dt.ToString("dd-MM-yyyy");
                }
                if (dt == DateTime.MinValue)
                {
                    if (DateTime.TryParseExact(strDate.Trim(), "d. MMMM yyyy", ci, DateTimeStyles.None, out dt))
                    {
                        datString = dt.ToString("dd-MM-yyyy");
                    }
                }
                //DateTime dateFormat1 = DateTime.ParseExact(strDate.Trim(), "dd. MMMM yyyy", ci);
                //if (dateFormat1 != DateTime.MinValue)
                //{
                //    //Check date of extracted item, if date is equal to current date or not 
                //   bool isLatest = Common.CheckDate(dateFormat1.ToString(), false);
                //    if (isLatest)
                //    {
                //        datString = dateFormat1.ToString("dd-MM-yyyy");
                //    }
                //}else
                //{
                //    DateTime dateFormat2 = DateTime.ParseExact(strDate.Trim(), "d. MMMM yyyy", ci);
                //    if (dateFormat2 != DateTime.MinValue)
                //    {
                //        //Check date of extracted item, if date is equal to current date or not 
                //        bool isLatest = Common.CheckDate(dateFormat2.ToString(), false);
                //        if (isLatest)
                //        {
                //            datString = dateFormat2.ToString("dd-MM-yyyy");
                //        }
                //    }
                //}
            }
            catch (Exception)
            {

            }
            return datString;
        }

        public static string GetDate(string strDate)
        {
            string date = string.Empty;
            bool foundMatch = false;
            string DateRegEx =
                 @"(?i-x)\b(?:(?:3[01]|[12][0-9]|0?[1-9])[\t ]+(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+[0-9]{4}|(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+(?:3[01]|[12][0-9]|0?[1-9]),[\t ]+[0-9]{4}|(?:3[01]|[12][0-9]|0?[1-9])\.[\t ]+(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+[0-9]{4}|(?:3[01]|[12][0-9]|0?[1-9])[/.-](?:1[0-2]|0?[1-9])[/.-][0-9]{4}|(?:1[0-2]|0?[1-9])[/.-](?:3[01]|[12][0-9]|0?[1-9])[/.-][0-9]{4}|[0-9]{4}[/.-](?:1[0-2]|0?[1-9])[/.-](?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])-(?:1[0-2]|0?[1-9])-[0-9]{4}|(?:1[0-2]|0?[1-9])-(?:3[01]|[12][0-9]|0?[1-9])-[0-9]{4}|[0-9]{4}-(?:1[0-2]|0?[1-9])-(?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])\.(?:1[0-2]|0?[1-9])\.[0-9]{4}|(?:1[0-2]|0?[1-9])\.(?:3[01]|[12][0-9]|0?[1-9])\.[0-9]{4}|[0-9]{4}\.(?:1[0-2]|0?[1-9])\.(?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])(?:3[01]|[12][0-9]|0?[1-9])-(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)-[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2})\b";
            // @"\b(?:(?:3[01]|[12][0-9]|0?[1-9])[\t ]+(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+[0-9]{4}|(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+(?:3[01]|[12][0-9]|0?[1-9]),[\t ]+[0-9]{4}|(?:3[01]|[12][0-9]|0?[1-9])\.[\t ]+(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+[0-9]{4}|(?:3[01]|[12][0-9]|0?[1-9])[/.-](?:1[0-2]|0?[1-9])[/.-][0-9]{4}|(?:1[0-2]|0?[1-9])[/.-](?:3[01]|[12][0-9]|0?[1-9])[/.-][0-9]{4}|[0-9]{4}[/.-](?:1[0-2]|0?[1-9])[/.-](?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])-(?:1[0-2]|0?[1-9])-[0-9]{4}|(?:1[0-2]|0?[1-9])-(?:3[01]|[12][0-9]|0?[1-9])-[0-9]{4}|[0-9]{4}-(?:1[0-2]|0?[1-9])-(?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])\.(?:1[0-2]|0?[1-9])\.[0-9]{4}|(?:1[0-2]|0?[1-9])\.(?:3[01]|[12][0-9]|0?[1-9])\.[0-9]{4}|[0-9]{4}\.(?:1[0-2]|0?[1-9])\.(?:3[01]|[12][0-9]|0?[1-9]))\b";
            try
            {
                //var ress = Regex.Matches(strDate, DateRegEx, RegexOptions.Singleline);
                Regex regex = new Regex(DateRegEx);
                Match match = regex.Match(strDate);
                foundMatch = Regex.IsMatch(strDate, @"(?i-x)\b(?:(?:3[01]|[12][0-9]|0?[1-9])[\t ]+(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+[0-9]{4}|(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+(?:3[01]|[12][0-9]|0?[1-9]),[\t ]+[0-9]{4}|(?:3[01]|[12][0-9]|0?[1-9])\.[\t ]+(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)[\t ]+[0-9]{4}|(?:3[01]|[12][0-9]|0?[1-9])[/.-](?:1[0-2]|0?[1-9])[/.-][0-9]{4}|(?:1[0-2]|0?[1-9])[/.-](?:3[01]|[12][0-9]|0?[1-9])[/.-][0-9]{4}|[0-9]{4}[/.-](?:1[0-2]|0?[1-9])[/.-](?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])-(?:1[0-2]|0?[1-9])-[0-9]{4}|(?:1[0-2]|0?[1-9])-(?:3[01]|[12][0-9]|0?[1-9])-[0-9]{4}|[0-9]{4}-(?:1[0-2]|0?[1-9])-(?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])\.(?:1[0-2]|0?[1-9])\.[0-9]{4}|(?:1[0-2]|0?[1-9])\.(?:3[01]|[12][0-9]|0?[1-9])\.[0-9]{4}|[0-9]{4}\.(?:1[0-2]|0?[1-9])\.(?:3[01]|[12][0-9]|0?[1-9])|(?:3[01]|[12][0-9]|0?[1-9])(?:3[01]|[12][0-9]|0?[1-9])-(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)(?:Jan|January|januar|Feb|febr|februar|February|Mar|March|marts|Apr|April|May|May|Maj|Jun|June|Juni|Jul|July|Juli|Aug|August|Sep|Sept|September|Oct|Okt|October|Oktober|Nov|November|Dec|December)-[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2})\b");

                if (foundMatch)
                {
                    date = match.Value;
                }
            }
            catch (ArgumentException ex)
            {
                // Syntax error in the regular expression
            }

            return date;
        }


        public static bool IsLatestDate(string strDate, string strDateWithSpecial)
        {
            bool isLatest = false;
            string fomattedDate = string.Empty;
            strDate = RemoveSpecialCharacters(strDateWithSpecial);
            fomattedDate = IsFormattedDate(strDate);
            if (!string.IsNullOrEmpty(fomattedDate))
            {
                strDate = fomattedDate;
            }
            //Check date of extracted item, if date is equal to current date or not 
            isLatest = CheckDate(strDate, false);
            return isLatest;
        }

    }
}
