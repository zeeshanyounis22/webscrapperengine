﻿using Scrapper.Model.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scrapper.Utility.Util
{
    public class EmailHelper
    {
        public static void SendEmail(EmailMessageModel model)
        {
            //Create a new message and dispose MailMessage object
            using (var message = new MailMessage())
            {
                message.Subject = model.Subject;
                message.Body = model.Body;
                message.IsBodyHtml = true;
                message.SubjectEncoding = Encoding.ASCII;
                message.Priority = MailPriority.Normal;

                //Add recipient
                if (!string.IsNullOrEmpty(model.To))
                {
                    if (model.To.Contains(","))
                    {
                        foreach (var address in model.To.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            message.To.Add(address);
                            //Send email and dispose SmtpClient object
                            using (var smtpClient = new SmtpClient())
                            {
                                try
                                {
                                    smtpClient.Send(message);
                                }
                                catch (SmtpFailedRecipientException ex)
                                {
                                    var statusCode = ex.StatusCode;
                                    if (statusCode == SmtpStatusCode.MailboxBusy || statusCode == SmtpStatusCode.MailboxUnavailable ||
                                        statusCode == SmtpStatusCode.TransactionFailed)
                                    {
                                        // wait 10 seconds, try a second time
                                        Thread.Sleep(5000);
                                        smtpClient.Send(message);
                                    }
                                    else
                                    {
                                        //We don't have any of the above exception
                                        //Lets throw it, Outer Try Catch will catch it
                                        throw;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        message.To.Add(model.To);
                        //Send email and dispose SmtpClient object
                        using (var smtpClient = new SmtpClient())
                        {
                            try
                            {
                                smtpClient.Send(message);
                            }
                            catch (SmtpFailedRecipientException ex)
                            {
                                var statusCode = ex.StatusCode;
                                if (statusCode == SmtpStatusCode.MailboxBusy || statusCode == SmtpStatusCode.MailboxUnavailable ||
                                    statusCode == SmtpStatusCode.TransactionFailed)
                                {
                                    // wait 10 seconds, try a second time
                                    Thread.Sleep(5000);
                                    smtpClient.Send(message);
                                }
                                else
                                {
                                    //We don't have any of the above exception
                                    //Lets throw it, Outer Try Catch will catch it
                                    throw;
                                }
                            }
                        }
                    }
                }

            }
        }

        public static void Send(EmailMessageModel model)
        {

            MailMessage message = new MailMessage();
            message.To.Add(model.To);
            message.Subject = model.Subject;
            message.IsBodyHtml = true;
            message.SubjectEncoding = Encoding.ASCII;
            message.Priority = MailPriority.Normal;
            message.Body = model.Body;
            var smtpClient = new SmtpClient();

            try
            {
                smtpClient.Send(message);
            }
            catch (SmtpFailedRecipientException ex)
            {
                var statusCode = ex.StatusCode;
                if (statusCode == SmtpStatusCode.MailboxBusy || statusCode == SmtpStatusCode.MailboxUnavailable ||
                    statusCode == SmtpStatusCode.TransactionFailed)
                {
                    // wait 10 seconds, try a second time
                    Thread.Sleep(5000);
                    smtpClient.Send(message);
                }
                else
                {
                    //We don't have any of the above exception
                    //Lets throw it, Outer Try Catch will catch it
                    throw;
                }
            }


        }
    }
}
