﻿using Scrapper.Model.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrapper.Utility.Util
{
    public class AccommodationImageModelComparer : IEqualityComparer<NewsModel>
    {
        public bool Equals(NewsModel x, NewsModel y)
        {
            if (x == null && y == null)
                return true;

            return x.Heading == y.Heading;
        }

        public int GetHashCode(NewsModel model)
        {
            return model.Heading.GetHashCode();
        }
    }
}
