﻿using Scrapper.Model.Html;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Scrapper.Utility.Html
{
    public static class HtmlTags
    {
        /// <summary>
        /// Matching node from given input
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static string FindNode(string node)
        {
            var listTags = Tags();
            string name = string.Empty;
            var htmlNode = listTags.Where(x => x.TagName == node).FirstOrDefault();
            if (htmlNode != null)
            {
                name = htmlNode.TagName;
                return name;
            }
            return name;

        }

        public static List<HtmlTagModel> Tags()
        {
            var lstTags = new List<HtmlTagModel>();
            lstTags.Add(new HtmlTagModel() { TagName = "div" });
            lstTags.Add(new HtmlTagModel() { TagName = "li" });
            lstTags.Add(new HtmlTagModel() { TagName = "article" });
            lstTags.Add(new HtmlTagModel() { TagName = "section" });
            lstTags.Add(new HtmlTagModel() { TagName = "small" });
            lstTags.Add(new HtmlTagModel() { TagName = "time" });
            lstTags.Add(new HtmlTagModel() { TagName = "span" });
            lstTags.Add(new HtmlTagModel() { TagName = "a" }); ;
            lstTags.Add(new HtmlTagModel() { TagName = "h3" });
            lstTags.Add(new HtmlTagModel() { TagName = "h1" });
            lstTags.Add(new HtmlTagModel() { TagName = "p" });
            lstTags.Add(new HtmlTagModel() { TagName = "#text" });
            lstTags.Add(new HtmlTagModel() { TagName = "strong" });
            lstTags.Add(new HtmlTagModel() { TagName = "date" });
            return lstTags;
        }

    }
}
